<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\AboutUs;

class AboutController extends Controller
{
    
    public function index()
    {
        $abouts = AboutUs::latest()->get();
        return view('backend.pages.abouts.index', compact('abouts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.abouts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:191',
            'details' => 'required',
            'image' => 'required|image|mimes:jpg,png,gif|max:100',
        ]);
        
        $about = new AboutUs(); 
        $about->user_id = Auth::user()->id;
        $about->title = $request->title;
        $about->details = $request->details;
        $img = $request->file('image');
        if($img){
            $imgName = date('YmdHi').$img->getClientOriginalName();
            $img->move('backend/images/abouts/', $imgName);
            $about['image'] = $imgName;
        }
        $about->save();
        
        $notification=array(
            'message'=>'About Added Successfully',
            'alert-type'=>'success'
        );
        return Redirect()->back()->with($notification);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $about = AboutUs::findOrFail($id);
        return view('backend.pages.abouts.edit', compact('about'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required|max:191',
            'details' => 'required',
            'image' => 'image|mimes:jpg,png,gif|max:100',
        ]);
        
        $about = AboutUs::findOrfail($id);
        $about->user_id = Auth::user()->id;
        $about->title = $request->title;
        $about->details = $request->details;
        $img = $request->file('image');
        if($img){
            $imgName = date('YmdHi').$img->getClientOriginalName();
            $img->move('backend/images/abouts/', $imgName);
            if(file_exists('backend/images/abouts/'.$about->image) AND !empty($about->image)){
                unlink('backend/images/abouts/'.$about->image);
            }
            $about['image'] = $imgName;
        }
        $about->save();
        
        $notification=array(
            'message'=>'About Updated Successfully',
            'alert-type'=>'success'
        );
        return Redirect()->route('admin.abouts')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $about = AboutUs::find($request->id);
        if(!is_null($about)){
            if(file_exists('backend/images/abouts/'.$about->image) AND !empty($about->image)){
                unlink('backend/images/abouts/'.$about->image);
            }
            
            $about->delete();
        }
        
        $notification=array(
            'message'=>'About Delete Successfully',
            'alert-type'=>'success'
        );
        return Redirect()->back()->with($notification);
    }
}
