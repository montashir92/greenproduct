<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Client;

class ClientController extends Controller
{
    
    public function index()
    {
        $clients = Client::latest()->get();
        return view('backend.pages.clients.index', compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.clients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'max:60',
            'image' => 'required|image|mimes:jpg,png,gif',
        ]);
        
        $client = new Client();
        $client->name = $request->name;
        
        $img = $request->file('image');
        if($img){
            $imgName = date('YmdHi').$img->getClientOriginalName();
            $img->move('backend/images/clients/', $imgName);
            $client['image'] = $imgName;
        } 
        $client->save();
        
        $notification=array(
            'message'=>'Client Added Successfully',
            'alert-type'=>'success'
        );
        return Redirect()->back()->with($notification);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client = Client::findOrFail($id);
        return view('backend.pages.clients.edit', compact('client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'max:60',
            'image' => 'image|mimes:jpg,png,gif',
        ]);
        
        $client = Client::findOrFail($id);
        $client->name = $request->name;
        
        $img = $request->file('image');
        if($img){
            $imgName = date('YmdHi').$img->getClientOriginalName();
            $img->move('backend/images/clients/', $imgName);
            if(file_exists('backend/images/clients/'.$client->image) AND !empty($client->image)){
                unlink('backend/images/clients/'.$client->image);
            }
            $client['image'] = $imgName;
        }
        
        $client->save();
        
        $notification=array(
            'message'=>'Client Updated Successfully',
            'alert-type'=>'success'
        );
        return Redirect()->route('admin.clients')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $client = Client::find($request->id);
        if(!is_null($client)){
            if(file_exists('backend/images/clients/'.$client->image) AND !empty($client->image)){
                unlink('backend/images/clients/'.$client->image);
            }
            $client->delete();
        }
        
        $notification=array(
            'message'=>'Client Deleted Successfully',
            'alert-type'=>'success'
        );
        return Redirect()->back()->with($notification);
    }
}
