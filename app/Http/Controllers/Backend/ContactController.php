<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Contact;

class ContactController extends Controller
{
    
    public function index()
    {
        $contacts = Contact::latest()->get();
        return view('backend.pages.contacts.index', compact('contacts'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $contact = Contact::find($request->id);
        if(!is_null($contact)){
            if(file_exists('backend/images/contacts/'.$contact->image) AND !empty($contact->image)){
                unlink('backend/images/contacts/'.$contact->image);
            }
            
            $contact->delete();
        }
        
        $notification=array(
            'message'=>'Contact Deleted Successfully',
            'alert-type'=>'success'
        );
        return Redirect()->back()->with($notification);
    }
}
