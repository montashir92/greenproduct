<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ImageGallery;

class ImagegalleryController extends Controller
{
    
    public function index()
    {
        $imagegalleries = ImageGallery::latest()->get();
        return view('backend.pages.imagegalleries.index', compact('imagegalleries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.imagegalleries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'max:100',
            'image' => 'required|image|mimes:jpg,jpeg,png,gif|max:100',
        ]);
        
        $gallery = new ImageGallery();
        $gallery->title = $request->title;
        $gallery->description = $request->description;
        
        $img = $request->file('image');
        if($img){
            $imgName = date('YmdHi').$img->getClientOriginalName();
            $img->move('backend/images/galleries/', $imgName);
            $gallery['image'] = $imgName;
        }
        
        $gallery->save();
        
        $notification=array(
            'message'=>'Image Gallery Added Successfully',
            'alert-type'=>'success'
        );
        return Redirect()->back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $imagegallery = ImageGallery::findOrFail($id);
        return view('backend.pages.imagegalleries.edit', compact('imagegallery'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'max:100',
            'image' => 'image|mimes:jpg,jpeg,png,gif|max:100',
        ]);
        
        $gallery = ImageGallery::findOrFail($id);
        $gallery->title = $request->title;
        $gallery->description = $request->description;
        
        $img = $request->file('image');
        if($img){
            $imgName = date('YmdHi').$img->getClientOriginalName();
            $img->move('backend/images/galleries/', $imgName);
            if(file_exists('backend/images/galleries/'.$gallery->image) AND !empty($gallery->image)){
                unlink('backend/images/galleries/'.$gallery->image);
            }
            $gallery['image'] = $imgName;
        }
        
        $gallery->save();
        
        $notification=array(
            'message'=>'Image Gallery Updated Successfully',
            'alert-type'=>'success'
        );
        return Redirect()->route('admin.photos')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $gallery = ImageGallery::find($request->id);
        if(!is_null($gallery)){
            if(file_exists('backend/images/galleries/'.$gallery->image) AND !empty($gallery->image)){
                unlink('backend/images/galleries/'.$gallery->image);
            }
            
            $gallery->delete();
        }
        
        $notification=array(
            'message'=>'Image Gallery Deleted Successfully',
            'alert-type'=>'success'
        );
        return Redirect()->back()->with($notification);
    }
}
