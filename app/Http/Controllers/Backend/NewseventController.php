<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Newwsevent;
use Illuminate\Support\Facades\Auth;

class NewseventController extends Controller
{
    
    public function index()
    {
        $newsevents = Newwsevent::latest()->get();
        return view('backend.pages.newsevents.index', compact('newsevents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.newsevents.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'        => 'required|max:100',
            'short_desc'   => 'required',
            'image'        => 'required|image|mimes:jpg,png,gif|max:100',
        ]);
        
        $newsevent = new Newwsevent();
        $newsevent->user_id      = Auth::user()->id;
        $newsevent->title        = $request->title;
        $newsevent->short_desc   = $request->short_desc;
        $newsevent->long_desc    = $request->long_desc;
        
        $img = $request->file('image');
        if($img){
            $imgName = date('YmdHi').$img->getClientOriginalName();
            $img->move('backend/images/newsevent/', $imgName);
            $newsevent['image'] = $imgName;
        }
        
        $newsevent->save();
        
        $notification=array(
            'message'=>'News & Event Added Successfully',
            'alert-type'=>'success'
        );
        return Redirect()->back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $newsevent = Newwsevent::findOrFail($id);
        return view('backend.pages.newsevents.edit', compact('newsevent'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'max:100',
            'short_desc' => 'required',
            'image' => 'image|mimes:jpg,png,gif|max:100',
        ]);
        
        $newsevent = Newwsevent::findOrFail($id);
        $newsevent->user_id = Auth::user()->id;
        $newsevent->title = $request->title;
        $newsevent->short_desc = $request->short_desc;
        $newsevent->long_desc = $request->long_desc;
        
        $img = $request->file('image');
        if($img){
            $imgName = date('YmdHi').$img->getClientOriginalName();
            $img->move('backend/images/newsevent/', $imgName);
            if(file_exists('backend/images/newsevent/'.$newsevent->image) AND !empty($newsevent->image)){
                unlink('backend/images/newsevent/'.$newsevent->image);
            }
            $newsevent['image'] = $imgName;
        }
        
        $newsevent->save();
        
        $notification=array(
            'message'=>'News & Event Updated Successfully',
            'alert-type'=>'success'
        );
        return Redirect()->route('admin.newsevents')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $newsevent = Newwsevent::find($request->id);
        if(!is_null($newsevent)){
            if(file_exists('backend/images/newsevent/'.$newsevent->image) AND !empty($newsevent->image)){
                unlink('backend/images/newsevent/'.$newsevent->image);
            }
            
            $newsevent->delete();
        }
        
        $notification=array(
            'message'=>'News & Event Deleted Successfully',
            'alert-type'=>'success'
        );
        return Redirect()->back()->with($notification);
    }
}
