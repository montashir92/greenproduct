<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Ourteam;


class OurteamController extends Controller
{
    
    public function index()
    {
        $ourteams = Ourteam::latest()->get();
        return view('backend.pages.ourteams.index', compact('ourteams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.ourteams.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:60',
            'designation' => 'required|max:50',
            'description' => 'required',
            'image' => 'required|image|mimes:jpg,png,gif|max:100',
            'twitter' => 'url|max:100',
            'facebook' => 'url|max:100',
            'instagram' => 'url|max:100',
            'linkedin' => 'url|max:100',
        ]);
        
        $team = new Ourteam();
        $team->name = $request->name;
        $team->designation = $request->designation;
        $team->description = $request->description;
        $team->twitter = $request->twitter;
        $team->facebook = $request->facebook;
        $team->instagram = $request->instagram;
        $team->linkedin = $request->linkedin;
        
        $img = $request->file('image');
        if($img){
            $imgName = date('YmdHi').$img->getClientOriginalName();
            $img->move('backend/images/teams/', $imgName);
            $team['image'] = $imgName;
        }
        
        $team->save();
        
        $notification=array(
            'message'=>'Team Added Successfully',
            'alert-type'=>'success'
        );
        return Redirect()->back()->with($notification);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ourteam = Ourteam::findOrFail($id);
        return view('backend.pages.ourteams.edit', compact('ourteam'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:100',
            'designation' => 'required|max:100',
            'description' => 'required',
            'image' => 'image|mimes:jpg,png,gif|max:100',
            'twitter' => 'url|max:100',
            'facebook' => 'url|max:100',
            'instagram' => 'url|max:100',
            'linkedin' => 'url|max:100',
        ]);
        
        $team = Ourteam::findOrFail($id);
        $team->name = $request->name;
        $team->designation = $request->designation;
        $team->description = $request->description;
        $team->twitter = $request->twitter;
        $team->facebook = $request->facebook;
        $team->instagram = $request->instagram;
        $team->linkedin = $request->linkedin;;
        
        $img = $request->file('image');
        if($img){
            $imgName = date('YmdHi').$img->getClientOriginalName();
            $img->move('backend/images/teams/', $imgName);
            if(file_exists('backend/images/teams/'.$team->image) AND !empty($team->image)){
                unlink('backend/images/teams/'.$team->image);
            }
            $team['image'] = $imgName;
        }
        
        $team->save();
        
        $notification=array(
            'message'=>'Team Updated Successfully',
            'alert-type'=>'success'
        );
        return Redirect()->route('admin.ourteams')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $team = Ourteam::find($request->id);
        if(!is_null($team)){
            if(file_exists('backend/images/teams/'.$team->image) AND !empty($team->image)){
                unlink('backend/images/teams/'.$team->image);
            }
            
            $team->delete();
        }
        
        $notification=array(
            'message'=>'Team Deleted Successfully',
            'alert-type'=>'success'
        );
        return Redirect()->back()->with($notification);
    }
}
