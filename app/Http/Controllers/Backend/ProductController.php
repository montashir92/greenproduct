<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\Category;

class ProductController extends Controller
{
    
    public function index()
    {
        $products = Product::latest()->get();
        return view('backend.pages.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::orderBy('name', 'ASC')->get();
        return view('backend.pages.products.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'category_id' => 'required',
            'name' => 'required|unique:products,name|max:100',
            'description' => 'required',
            'image' => 'required|image|mimes:jpg,png,gif|max:100',
        ]);
        
        $products = new Product();
        $products->user_id = Auth::user()->id;
        $products->category_id = $request->category_id;
        $products->name = $request->name;
        $products->description = $request->description;
        
        $img = $request->file('image');
        if($img){
            $imgName = date('YmdHi').$img->getClientOriginalName();
            $img->move('backend/images/products/', $imgName);
            $products['image'] = $imgName;
        }
        
        $products->save();
        
        //multiple image upload
        $files = $request->sub_image;
        if(!empty($files)){
            foreach ($files as $file) {
               $imageName = date('YmdHi').$file->getClientOriginalName();
               $file->move('backend/images/sub_products/', $imageName);
               $subImages['sub_image'] = $imageName;
               
               $subImages = new ProductImage();
               $subImages->product_id = $products->id;
               $subImages->sub_image = $imageName;
               $subImages->save();
            }
        }
        
        $notification=array(
            'message'=>'Product Added Successfully',
            'alert-type'=>'success'
        );
        return Redirect()->back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::findOrFail($id);
        $multiimages = ProductImage::where('product_id', $product->id)->latest()->get();
        return view('backend.pages.products.show', compact('product', 'multiimages'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::findOrFail($id);
        $categories = Category::orderBy('name', 'ASC')->get();
        $multiimages = ProductImage::where('product_id', $product->id)->latest()->get();
        return view('backend.pages.products.edit', compact('product', 'categories', 'multiimages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'category_id' => 'required',
            'name' => 'required|max:100',
            'description' => 'required',
            'image' => 'image|mimes:jpg,jpeg,png,gif|max:100',
        ]);
        
        $products = Product::findOrFail($id);
        $products->user_id = Auth::user()->id;
        $products->category_id = $request->category_id;
        $products->name = $request->name;
        $products->description = $request->description;
        
        $img = $request->file('image');
        if($img){
            $imgName = date('YmdHi').$img->getClientOriginalName();
            $img->move('backend/images/products/', $imgName);
            if(file_exists('backend/images/products/'.$products->image) AND !empty($products->image)){
                unlink('backend/images/products/'.$products->image);
            }
            $products['image'] = $imgName;
        }
        
        $products->save();
        
        //multiple image upload
        $files = $request->sub_image;
        if(!empty($files)){
            $subImages = ProductImage::where('product_id', $id)->get()->toArray();
            
            foreach ($subImages as $value) {
                if(!empty($value)){
                    unlink('backend/images/sub_products/'.$value['sub_image']);
                }
            }
            ProductImage::where('product_id', $id)->delete();
        }
        if(!empty($files)){
            foreach ($files as $file) {
               $imageName = date('YmdHi').$file->getClientOriginalName();
               $file->move('backend/images/sub_products/', $imageName);
               $subImages['sub_image'] = $imageName;
               
               $subImages = new ProductImage();
               $subImages->product_id = $products->id;
               $subImages->sub_image = $imageName;
               $subImages->save();
            }
        }
        
        $notification=array(
            'message'=>'Product Updated Successfully',
            'alert-type'=>'success'
        );
        return Redirect()->route('admin.products')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $product = Product::find($request->id);
        if(!is_null($product)){
            if(file_exists('backend/images/products/'.$product->image) AND !empty($product->image)){
                unlink('backend/images/products/'.$product->image);
            }
            
            $subImages = ProductImage::where('product_id', $product->id)->get()->toArray();
            if(!empty($subImages)){
                foreach ($subImages as $value) {
                    if(!empty($value)){
                        unlink('backend/images/sub_products/'.$value['sub_image']);
                    }
                }
            }
            
            ProductImage::where('product_id', $product->id)->delete();
            $product->delete();
        }
        
        $notification=array(
            'message'=>'Product Deleted Successfully',
            'alert-type'=>'success'
        );
        return Redirect()->back()->with($notification);
    }
}
