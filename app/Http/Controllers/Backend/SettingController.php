<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Setting;

class SettingController extends Controller
{
    
    public function index()
    {
        $settings = Setting::latest()->get();
        return view('backend.pages.settings.index', compact('settings'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $setting = Setting::findOrFail($id);
        return view('backend.pages.settings.edit', compact('setting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'phone' => 'required|max:15',
            'email' => 'required|email|max:100',
            'address' => 'required|max:191',
            'map' => 'required|max:500',
        ]);
        
        $setting = Setting::findOrFail($id);
        $setting->phone = $request->phone;
        $setting->email = $request->email;
        $setting->address = $request->address;
        $setting->map = $request->map;
        $setting->save();
        
        $notification=array(
            'message'=>'Setting Updated Succefully..',
            'alert-type'=>'success'
        );
        return Redirect()->back()->with($notification);
    }

}
