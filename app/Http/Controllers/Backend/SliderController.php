<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Slider;

class SliderController extends Controller
{
    
    public function index()
    {
        $sliders = Slider::latest()->get();
        return view('backend.pages.sliders.index', compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.sliders.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|unique:sliders,title|max:100',
            'description' => 'required',
            'image' => 'required|image|mimes:jpg,png,gif|max:3000|dimensions:min_width=1345,height=580',
        ]);
        
        $sliders = new Slider();
        $sliders->title = $request->title;
        $sliders->description = $request->description;
        $img = $request->file('image');
        if($img){
            $imgName = date('YmdHi').$img->getClientOriginalName();
            $img->move('backend/images/sliders/', $imgName);
            $sliders['image'] = $imgName;
        }
        $sliders->save();
        
        $notification=array(
            'message'=>'Slider Added Successfully',
            'alert-type'=>'success'
        );
        return Redirect()->back()->with($notification);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider = Slider::findOrFail($id);
        return view('backend.pages.sliders.edit', compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required|max:100',
            'description' => 'required',
            'image' => 'image|mimes:jpg,png,gif|max:3000|dimensions:min_width=1345,height=580',
        ]);
        
        $sliders = Slider::findOrFail($id);
        $sliders->title = $request->title;
        $sliders->description = $request->description;
        $img = $request->file('image');
        if($img){
            $imgName = date('YmdHi').$img->getClientOriginalName();
            $img->move('backend/images/sliders/', $imgName);
            if(file_exists('backend/images/sliders/'.$sliders->image) AND !empty($sliders->image)){
                unlink('backend/images/sliders/'.$sliders->image);
            }
            $sliders['image'] = $imgName;
        }
        $sliders->save();
        
        $notification=array(
            'message'=>'Slider Updated Successfully',
            'alert-type'=>'success'
        );
        return Redirect()->route('admin.sliders')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $slider = Slider::find($request->id);
        if(!is_null($slider)){
            if(file_exists('backend/images/sliders/'.$slider->image) AND !empty($slider->image)){
                unlink('backend/images/sliders/'.$slider->image);
            }
            $slider->delete();
            
        }
        
        $notification=array(
            'message'=>'Slider Deleted Successfully',
            'alert-type'=>'success'
        );
        return Redirect()->back()->with($notification);
    }
}
