<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Contact;

class ContactsController extends Controller
{
    
    public function store(Request $request)
    {
        
        $this->validate($request, [
            'name' => 'required|max:60',
            'email' => 'required|email|unique:contacts,email|max:100',
            'subject' => 'required|max:100',
            'message' => 'required',
        ]);
        
        $contact = new Contact();
        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->subject = $request->subject;
        $contact->message = $request->message;
        $contact->save();
        
        $notification=array(
            'message'=>'Sucessfully Send Message...',
            'alert-type'=>'success'
        );
        return Redirect()->back()->with($notification);
    }

}
