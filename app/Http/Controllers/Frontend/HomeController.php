<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Slider;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\Setting;
use App\Models\Ourteam;
use App\Models\ImageGallery;
use App\Models\Newwsevent;
use App\Models\Client;
use App\Models\AboutUs;

class HomeController extends Controller
{
    
    public function index()
    {
        $data['sliders'] = Slider::orderBy('id', 'ASC')->get();
        $data['categories'] = Category::orderBy('name', 'ASC')->get();
        $data['products'] = Product::where('status', 1)->orderBy('id', 'DESC')->get();
        $data['new_products'] = Product::where('status', 1)->orderBy('id', 'DESC')->limit(8)->get();
        $data['clients'] = Client::orderBy('id', 'ASC')->get();
        $data['teams'] = Ourteam::orderBy('id', 'DESC')->get();
        $data['about'] = AboutUs::find(1);
        return view('frontend.pages.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function about()
    {
        $teams = Ourteam::orderBy('id', 'DESC')->get();
        $about = AboutUs::find(1);
        return view('frontend.pages.about', compact('teams', 'about'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function team()
    {
        $teams = Ourteam::orderBy('id', 'DESC')->get();
        return view('frontend.pages.team', compact('teams'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getProduct()
    {
        $products = Product::orderBy('id', 'DESC')->get();
        return view('frontend.pages.service', compact('products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function galleryImage()
    {
        $photogalleries = ImageGallery::orderBy('id', 'DESC')->get();
        return view('frontend.pages.gallery_image', compact('photogalleries'));
    }
    
    /*
     * Blog Page Method
     */
    public function blog()
    {
        $blogs = Newwsevent::orderBy('id', 'DESC')->paginate(6);
        return view('frontend.pages.bloging', compact('blogs'));
    }
    
    /*
     * Blog Page Method
     */
    public function blogDetails($id)
    {
        $blog = Newwsevent::findorFail($id);
        $recent_posts = Newwsevent::orderBy('id', 'DESC')->limit(6)->get();
        return view('frontend.pages.blog_details', compact('blog', 'recent_posts'));
    }

    /*
     * Conatct Pasge Method
     */
    public function contact()
    {
        $setting = Setting::find(3);
        return view('frontend.pages.contact', compact('setting'));
    }
    
    /*
     * Product Details Method
     */
    public function show($id)
    {
        $data['product'] = Product::findOrFail($id);
        $data['product_images'] = ProductImage::where('product_id', $id)->get();
        return view('frontend.pages.product.show', $data);
    }
}
