<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('backend.pages.index');
    }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function profile()
    {
        return view('users.index');
    }
    
    /*
     *  Profile Update Method
     */
    public function profileUpdate(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required|numeric',
            'image' => 'image|mimes:jpg,png,gif',
        ]);

        $user = User::findOrFail(Auth::id());
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $image = $request->file('image');
        if($image){
            $imgName = date('YmdHi').$image->getClientOriginalName();
            $image->move('backend/images/users', $imgName);
            if(file_exists('backend/images/users'.$user->image) AND !empty($user->image)){
                unlink('backend/images/users'.$user->image);
            }
            $user->image = $imgName;
        }
        $user->save();

        $notification=array(
            'message'=>'Your Profile Updated Successfully',
            'alert-type'=>'success'
        );
        return Redirect()->back()->with($notification);
    }
    
    /*
     * Change Password Method
     */
    public function editPassword()
    {
        return view('users.change_password');
    }
    
    /*
     * Update Password Method
     */
    public function updatePassword(Request $request)
    {
        $request->validate([
            'old_password' => 'required',
            'new_password' => 'required|min:8',
            'password_confirmation' => 'required|min:8',
        ]);
        
        if(Auth::attempt(['id' => Auth::user()->id, 'password' => $request->old_password])){
            $user = User::findOrFail(Auth::id());
            $user->password = Hash::make($request->new_password);
            $user->save();
            
            Auth::logout();
            
            $notification=array(
              'message'=>'Your Password Change Success. Now Login With New Password',
              'alert-type'=>'success'
            );
            return Redirect()->route('login')->with($notification);
        }else{
            $notification=array(
                'message'=>'Old Password Not Match',
                'alert-type'=>'error'
            );
            return Redirect()->back()->with($notification);
        }
    }
}
