<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name', 60);
            $table->string('email', 100)->unique();
            $table->string('phone', 15);
            $table->string('username', 60);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('image', 100)->nullable();
            $table->boolean('status')->default(1);
            $table->integer('saved_by')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
        
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
