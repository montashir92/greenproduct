-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 14, 2022 at 11:31 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_greenproduct`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_us`
--

CREATE TABLE `about_us` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `about_us`
--

INSERT INTO `about_us` (`id`, `user_id`, `title`, `details`, `image`, `created_at`, `updated_at`) VALUES
(1, 1, 'EUM IPSAM LABORUM DELENITI VELITENA', '<p style=\"box-sizing: border-box; margin-bottom: 1rem; color: rgb(77, 70, 67); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px;\">Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p><ul style=\"box-sizing: border-box; padding: 0px; list-style: none; color: rgb(77, 70, 67); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px;\"><li style=\"box-sizing: border-box; padding: 10px 0px 0px 28px; position: relative;\"><span class=\"ri-check-double-line\" style=\"box-sizing: border-box; -webkit-font-smoothing: antialiased; left: 0px; top: 7px; position: absolute; font-size: 20px; color: rgb(27, 189, 54); font-family: remixicon !important;\"></span>Ullamco laboris nisi ut aliquip ex ea commodo consequa</li><li style=\"box-sizing: border-box; padding: 10px 0px 0px 28px; position: relative;\"><span class=\"ri-check-double-line\" style=\"box-sizing: border-box; -webkit-font-smoothing: antialiased; left: 0px; top: 7px; position: absolute; font-size: 20px; color: rgb(27, 189, 54); font-family: remixicon !important;\"></span>Duis aute irure dolor in reprehenderit in voluptate velit</li><li style=\"box-sizing: border-box; padding: 10px 0px 0px 28px; position: relative;\"><span class=\"ri-check-double-line\" style=\"box-sizing: border-box; -webkit-font-smoothing: antialiased; left: 0px; top: 7px; position: absolute; font-size: 20px; color: rgb(27, 189, 54); font-family: remixicon !important;\"></span>Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in</li></ul><p class=\"fst-italic\" style=\"box-sizing: border-box; margin-bottom: 0px; color: rgb(77, 70, 67); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; font-style: italic !important;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>', '202202100406team-3.jpg', NULL, '2022-02-09 22:23:46');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(4, 'Black Olives', 'black-olives', '2022-02-09 04:15:53', '2022-02-09 04:15:53'),
(5, 'Cured Olives', 'cured-olives', '2022-02-09 04:16:03', '2022-02-09 04:16:03'),
(6, 'Green Varieties', 'green-varieties', '2022-02-09 04:16:17', '2022-02-09 04:16:17'),
(7, 'Black Varieties', 'black-varieties', '2022-02-09 04:16:31', '2022-02-09 04:16:31'),
(8, 'Storing Olives', 'storing-olives', '2022-02-09 04:16:40', '2022-02-09 04:16:40');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `name`, `image`, `created_at`, `updated_at`) VALUES
(1, 'myob', '202202130919client-1.png', '2022-02-13 03:19:20', '2022-02-13 03:19:20'),
(2, 'belimo', '202202130920client-2.png', '2022-02-13 03:20:14', '2022-02-13 03:20:14'),
(3, 'Life Groups', '202202130920client-3.png', '2022-02-13 03:20:37', '2022-02-13 03:20:37'),
(4, 'pilly', '202202130921client-4.png', '2022-02-13 03:21:04', '2022-02-13 03:21:04'),
(5, 'citrus', '202202130921client-5.png', '2022-02-13 03:21:19', '2022-02-13 03:21:19'),
(6, 'Trustly', '202202130921client-6.png', '2022-02-13 03:21:34', '2022-02-13 03:21:34'),
(7, 'oldendroff', '202202130922client-7.png', '2022-02-13 03:22:15', '2022-02-13 03:22:15'),
(8, 'Grabyo', '202202130922client-8.png', '2022-02-13 03:22:38', '2022-02-13 03:22:38');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `subject`, `message`, `created_at`, `updated_at`) VALUES
(1, 'olive oil', 'olive@gmail.com', 'Olive Oil', 'sefsdf sd gsgs gs gs', '2022-02-10 04:57:16', '2022-02-10 04:57:16'),
(2, 'test category', 'test@gmail.com', 'Test', 'TestTestTestTestTest', '2022-02-10 04:58:42', '2022-02-10 04:58:42'),
(3, 'User', 'user@gmail.com', 'User', 'UserUserUserUserUserUserUserUser', '2022-02-10 05:03:19', '2022-02-10 05:03:19'),
(5, 'test category', 'admin@gmail.com', 'Olive Oil', 'test', '2022-02-13 04:20:07', '2022-02-13 04:20:07'),
(6, 'test category', 'montashir92@gmail.com', 'dfgdfg', 'fhvgjh', '2022-02-14 01:26:17', '2022-02-14 01:26:17');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `homepages`
--

CREATE TABLE `homepages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `image_galleries`
--

CREATE TABLE `image_galleries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `image_galleries`
--

INSERT INTO `image_galleries` (`id`, `title`, `description`, `image`, `created_at`, `updated_at`) VALUES
(1, 'title 1', 'fdhhdhdfh', '202202120934about.jpg', '2022-02-12 03:34:06', '2022-02-12 03:34:06'),
(2, 'title 2', 'fdgsdgsdg', '202202120934blog-1.jpg', '2022-02-12 03:34:51', '2022-02-12 03:34:51'),
(3, 'title 3', 'yjhfdfwkjhgfg', '202202120935blog-author.jpg', '2022-02-12 03:35:04', '2022-02-12 03:35:04'),
(4, 'title 4', 'esofj profjro fkorpjk', '202202120935blog-2.jpg', '2022-02-12 03:35:15', '2022-02-12 03:35:15'),
(5, 'title 5', 'fkop k kifiousiuhfsdiuhf', '202202120935blog-3.jpg', '2022-02-12 03:35:32', '2022-02-12 03:35:32'),
(6, 'title 6', 'dfg dgrftjsdf sfg s', '202202120935blog-4.jpg', '2022-02-12 03:35:43', '2022-02-12 03:35:43'),
(7, 'title 7', 'kdfiyejfggff nfdfjiorjtio v', '202202120936blog-recent-3.jpg', '2022-02-12 03:36:01', '2022-02-12 03:36:01'),
(8, 'title 8', 'rtr tfewtwetew', '202202120936blog-recent-2.jpg', '2022-02-12 03:36:23', '2022-02-12 03:36:23');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_02_08_091519_create_categories_table', 1),
(6, '2022_02_08_091744_create_sliders_table', 1),
(7, '2022_02_08_091925_create_products_table', 1),
(8, '2022_02_08_092542_create_homepages_table', 1),
(9, '2022_02_08_092713_create_about_us_table', 1),
(11, '2022_02_08_094003_create_product_images_table', 1),
(12, '2022_02_08_092925_create_contacts_table', 2),
(13, '2022_02_12_042959_create_settings_table', 3),
(14, '2022_02_12_060522_create_services_table', 4),
(15, '2022_02_12_065316_create_ourteams_table', 5),
(16, '2022_02_12_085938_create_image_galleries_table', 6),
(17, '2022_02_12_111543_create_newwsevents_table', 7),
(18, '2022_02_13_085520_create_clients_table', 8);

-- --------------------------------------------------------

--
-- Table structure for table `newwsevents`
--

CREATE TABLE `newwsevents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_desc` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `long_desc` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `newwsevents`
--

INSERT INTO `newwsevents` (`id`, `user_id`, `title`, `short_desc`, `long_desc`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Where does it come from', '<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover</span>', '<p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>', '202202130427blog-recent-5.jpg', 1, '2022-02-12 22:27:19', '2022-02-12 22:27:19'),
(2, 1, 'Why do we use it', '<span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet</span>', '<p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">\"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.\"</p><p style=\"margin-right: 0px; margin-bottom: 15px; margin-left: 0px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">\"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.\"</p>', '202202130428Aceitunas-525x360-1.jpg', 1, '2022-02-12 22:28:51', '2022-02-12 22:28:51'),
(3, 1, 'Extra Virgin Olive Oil', '<span style=\"color: rgb(102, 102, 102); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 16px;\">The&nbsp;</span><span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 16px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-weight: 700; color: rgb(102, 102, 102); font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">extra virgin olive oils</span><span style=\"color: rgb(102, 102, 102); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 16px;\">&nbsp;available come from different parts of Spain: Toledo, Córdoba, Almería, Tarragona, Jaen and Cáceres. Our extra virgin olive oils can be of different types depending on the&nbsp;</span><span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 16px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-weight: 700; color: rgb(102, 102, 102); font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">type of olive</span><span style=\"color: rgb(102, 102, 102); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 16px;\">&nbsp;that is used: Arbequina, Picual, Hojiblanca, Cornicabra, Manzanilla, Coupage or organic.</span>', '<p style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; min-height: 1px; color: rgb(105, 105, 105); font-family: Hind, sans-serif;\">After the<strong style=\"box-sizing: border-box; font-weight: bold;\">&nbsp;picking process</strong>&nbsp;and once the olives are settled in the mills, the elaboration process starts.</p><p style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; min-height: 1px; color: rgb(105, 105, 105); font-family: Hind, sans-serif;\">The first step is the washing up of the olives in order to be classified.</p><p style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; min-height: 1px; color: rgb(105, 105, 105); font-family: Hind, sans-serif;\">The fact of&nbsp;<strong style=\"box-sizing: border-box; font-weight: bold;\">grinding the olives up to 24 hours</strong>&nbsp;after the picking process is essential for this top quality olive oil. Otherwise, olive oil may be oxidized during storage because of the fermentation of the olive.</p><p style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; min-height: 1px; color: rgb(105, 105, 105); font-family: Hind, sans-serif;\">By&nbsp;<strong style=\"box-sizing: border-box; font-weight: bold;\">separating the bone from the skin</strong>&nbsp;by the grinding process, the olive oil is released forming a smooth, thick paste. Without pause, the next step is to extract the olive oil and vegetable water by a cold pressing process, this process is known as decantation, and in this case is effected by centrifugal force.</p><p style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; min-height: 1px; color: rgb(105, 105, 105); font-family: Hind, sans-serif;\">The next step is the&nbsp;<strong style=\"box-sizing: border-box; font-weight: bold;\">cleaning of the resulting liquid</strong>&nbsp;achieved by adding some water. And with this and the olive oil deaeration by small decanters, the end of the process is reached. It is classified in containers and stored in stainless steel tanks.</p><p style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; min-height: 1px; color: rgb(105, 105, 105); font-family: Hind, sans-serif;\">This is how&nbsp;<a href=\"https://www.jamonarium.com/en/175-extra-virgin-olive-oil-vinegar-mas-tarres-siurana-tarragona-precio\" style=\"box-sizing: border-box; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-weight: bold; color: rgb(201, 27, 41) !important;\">Olis Solé</a>&nbsp;reaches an Extra Virgin Olive Oil, a natural olive oil without any chemical processing or other substances that could alter the flavor, aroma and vitamins of the fruit.&nbsp;&nbsp;</p><p style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; min-height: 1px; color: rgb(105, 105, 105); font-family: Hind, sans-serif;\"><br></p><p style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; min-height: 1px; color: rgb(105, 105, 105); font-family: Hind, sans-serif;\">he&nbsp;<strong style=\"box-sizing: border-box; font-weight: bold;\">container</strong>&nbsp;used partly determines the final quality of the olive oil. The most efficient packing&nbsp;are the<strong style=\"box-sizing: border-box; font-weight: bold;\">&nbsp;dark&nbsp;packaging</strong>&nbsp;as they ensure blocking light and air for the olive oil.</p><p style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; min-height: 1px; color: rgb(105, 105, 105); font-family: Hind, sans-serif;\">To maximize storage stability and maintain its properties intact,&nbsp;<strong style=\"box-sizing: border-box; font-weight: bold;\">dark glass</strong>&nbsp;is one of the ideal packaging for olive oil.</p><p style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; min-height: 1px; color: rgb(105, 105, 105); font-family: Hind, sans-serif;\">In order to preserve intact all its organoleptic qualities and to ensure the highest quality for the consumer,&nbsp;<a href=\"https://www.jamonarium.com/en/145-extra-virgin-olive-oil-spanish-oils-price\" style=\"box-sizing: border-box; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-weight: bold; color: rgb(201, 27, 41) !important;\">Olis Solé</a>&nbsp;has a bag in box format 2 liters.</p><p style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; min-height: 1px; color: rgb(105, 105, 105); font-family: Hind, sans-serif;\">Thanks to the<strong style=\"box-sizing: border-box; font-weight: bold;\">&nbsp;metallic bag and hermetic tap,</strong>&nbsp;direct contact with light and oxygen penetration inside the packaging is avoided, ensuring its preservation as long as the product is packaged.</p>', '2022021304323.1.1.MAS_TARRES_AOVE_DOPSiurana_Arbequina.jpg', 1, '2022-02-12 22:32:43', '2022-02-12 22:32:43'),
(4, 1, 'A long life to Organic Olive Oils', '<span style=\"color: rgb(102, 102, 102); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 16px;\">When we talk about organic Olive Oils, we are no longer talking about a growing trend among a group of people with exquisitely natural preferences. No, we are talking about an increasingly extended reality, which is already part of our daily life.</span>', '<p style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(102, 102, 102); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\"><span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent; color: rgb(0, 0, 0);\"><span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent;\">In Spain, the number of hectares dedicated entirely to&nbsp;</span><b style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent;\">organic production</b><span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent;\">&nbsp;is increasing. This means that the commitment to this type of product is on the rise.&nbsp;</span></span></p><p style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(102, 102, 102); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\"><span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent; color: rgb(0, 0, 0);\">When we talk about&nbsp;</span><b style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent; color: rgb(0, 0, 0);\">ecological, organic or bio Olive Oils</b><span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent; color: rgb(0, 0, 0);\">, we are talking about an olive oil whose production process has respected the&nbsp;</span><b style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent; color: rgb(0, 0, 0);\">best environmental practices</b><span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent; color: rgb(0, 0, 0);\">, in which no pesticides, plaguicides or chemical fertilizers have been used, and in which responsible use of resources has been made, always adjusting to the possibilities offered by the land. In this type of Oils, also, special attention has been paid to the process of olive ripening, harvesting and extraction.&nbsp;</span></p><p style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(102, 102, 102); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\"><span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent; color: rgb(0, 0, 0);\">This does not mean that other EVOOs are worse, you can find&nbsp;</span><b style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent; color: rgb(0, 0, 0);\">extraordinary Olive Oils</b><span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent; color: rgb(0, 0, 0);\">&nbsp;produced in a traditional way, but hey,&nbsp;</span><b style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent; color: rgb(0, 0, 0);\">organic Olive Oils</b><span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent; color: rgb(0, 0, 0);\">&nbsp;have that natural touch that the others do not have. And that translates into&nbsp;</span><b style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent; color: rgb(0, 0, 0);\">quality, flavor&nbsp;</b><span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent; color: rgb(0, 0, 0);\">and&nbsp;</span><b style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent; color: rgb(0, 0, 0);\">health</b><span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent; color: rgb(0, 0, 0);\">.</span></p><p style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(102, 102, 102); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\"><span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent; color: rgb(0, 0, 0);\"></span><span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent; text-decoration-line: underline;\"><a href=\"https://www.jamonarium.com/en/spanish-olive-oil-evoo/805-mas-tarres-arbequina-organic-500ml-extra-virgin-olive-oil.html\" style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(0, 0, 0);\"><b style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent;\">Mas Tarrés</b></a></span><b style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent; color: rgb(0, 0, 0);\">,&nbsp;</b><a href=\"https://www.jamonarium.com/en/spanish-olive-oil-evoo/729-oro-del-desierto-coupage-organic-500ml-extra-virgin-olive-oil.html\" style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(0, 0, 0);\"><b style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent;\"><span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent; text-decoration-line: underline;\">Oro del Desierto</span></b></a><b style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent; color: rgb(0, 0, 0);\">,&nbsp;</b><a href=\"https://www.jamonarium.com/en/almazaras-de-la-subbetica/1676-rincon-de-la-subbetica-hojiblanca-organic-500ml-extra-virgin-olive-oil-do-priego-de-cordoba.html\" style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(0, 0, 0);\"><b style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent;\"><span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent; text-decoration-line: underline;\">Rincón de la Subbética</span></b></a><b style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent; color: rgb(0, 0, 0);\">,&nbsp;</b><span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent; text-decoration-line: underline;\"><a href=\"https://www.jamonarium.com/en/spanish-olive-oil-evoo/1662-cortijo-de-suerte-alta-picual-en-envero-organic-1l-extra-virgin-olive-oil-do-baena.html\" style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(0, 0, 0);\"><b style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent;\">Cortijo de Suerte Alta</b></a></span><b style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent; color: rgb(0, 0, 0);\">,&nbsp;</b><span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent; color: rgb(0, 0, 0);\">or</span><span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent; color: rgb(0, 0, 0);\">&nbsp;</span><span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent; text-decoration-line: underline;\"><a href=\"https://www.jamonarium.com/en/spanish-olive-oil-evoo/1660-marques-de-prado-seleccion-familiar-coupage-organic-500ml-extra-virgin-olive-oil.html\" style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(0, 0, 0);\"><b style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent;\">Marqués de Prado</b></a></span><b style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent; color: rgb(0, 0, 0);\">.&nbsp;</b><span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent; color: rgb(0, 0, 0);\">These are some of the best organic Olive Oils in the market that you can find in our online store. Where we guarantee the authenticity with its corresponding labeling, including the seal and numerical control code required by the European Union for this type of products.&nbsp;</span></p><p style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(102, 102, 102); font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\"><span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent; color: rgb(0, 0, 0);\"><span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent;\">Let yourself be carried away by nature and discover the best&nbsp;</span><b style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent;\">organic Olive Oils</b><span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent;\">&nbsp;from us.</span><span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent; text-decoration-line: underline;\"><a href=\"https://www.jamonarium.com/en/gift-hampers/1706-pack-avoe-ecologicos-los-6-mejores-aceites-de-oliva-ecologicos.html\" style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(0, 0, 0);\">&nbsp;<span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent;\">(Buy organic Olive Oils)</span></a></span></span></p>', '202202130433IMG_3383.jpg', 1, '2022-02-12 22:33:52', '2022-02-12 22:33:52'),
(5, 1, 'Olive oil in the antique Crete', '<span style=\"color: rgb(102, 102, 102); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 16px;\">It’s important to first know that the Cretan are the biggest consumers of olive oil by people and by year in the world. Each resident consume about 30 liters, unlike to the french people who consume about 0.5 liter, this is a huge difference. Antique Crete was very...</span>', '<p style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(102, 102, 102); font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">It’s important to first know that the Cretan are the biggest consumers of&nbsp;<a href=\"https://www.jamonarium.com/en/cms/10/olive-oil-extra-virgin-tipes-elaboration\" style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(46, 163, 242);\">olive oil</a>&nbsp;by people and by year in the world. Each resident consume about 30 liters, unlike to the french people who consume about 0.5 liter, this is a huge difference.</p><p style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(102, 102, 102); font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Antique Crete was very religious, olive oil at the start hasn’t for first goal the culinary side, but rather to honor the gods.</p><p style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(102, 102, 102); font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Residents of Crete worship without limit to the gods, and a lot of ceremonies in favor of their gods were implemented.</p><p style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(102, 102, 102); font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Then olive oil was a business asset definite for Crete, indeed while needs of life didn’t exceed 2,000 hectoliters, some stores had up to 10,000 hectoliters of olive oil. It allowed them to export much olive oil and make it to the most important in Crete.</p><p style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(102, 102, 102); font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">The other cult by Cretan is the health. Indeed according to them olive oil would a lot of prolific virtues for the body. That was proved since.</p>', '202202130434GettyImages-81740527web-58a47d185f9b58819c9daf36.jpg', 1, '2022-02-12 22:34:47', '2022-02-12 22:34:47'),
(6, 1, 'When is the best time to pick the olives', '<span style=\"color: rgb(102, 102, 102); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 16px;\">It is the great unknown when the beginning of the campaign of the olive arrives. When is the best time to pick the olives? In recent years, early harvesting is supported but this time is always the subject of debate between olive growers and cooperatives, as each...</span>', '<p style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(102, 102, 102); font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">It is the great unknown when the beginning of the campaign of the&nbsp;<a href=\"https://www.jamonarium.com/en/145-extra-virgin-olive-oil-spanish-oils-price\" style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(46, 163, 242);\">olive</a>&nbsp;arrives. When is<span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent; font-weight: 700;\">&nbsp;the best time to pick the olives?</span></p><p style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(102, 102, 102); font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">In recent years, early harvesting is supported but this time is always the subject of debate between olive growers and cooperatives, as each season is different due to&nbsp;<span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent; font-weight: 700;\">environmental conditions.</span>&nbsp;The time of harvesting is a fundamental factor in the quality of olive oil, hence its importance and controversy.&nbsp;To get an idea of when is the best time for our harvest, we must take into account the Index of Ripening of our olives. The quality of the oil remains constant for a long period after ripening, provided the olive is kept in the tree. To start harvesting, it is advisable to follow the<span style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background: transparent; font-weight: 700;\">&nbsp;Maturity Index</span>&nbsp;(I.M.):</p><p style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(102, 102, 102); font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Class 0: Intense green skin.</p><p style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(102, 102, 102); font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Class 1: Yellowish-green skin.</p><p style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(102, 102, 102); font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Class 2: Green skin with reddish spots in less than half the fruit, beginning of veraison.</p><p style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(102, 102, 102); font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Class 3: Reddish or purple skin in more than half of the fruit, end of veraison.</p><p style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(102, 102, 102); font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Class 4: Black skin and white flesh.</p><p style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(102, 102, 102); font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Class 5: Black skin and purple pulp without reaching the middle of the pulp.</p><p style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(102, 102, 102); font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Class 6: Black skin and purple pulp without reaching the bone.</p><p style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: rgb(102, 102, 102); font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Class 7: Black skin and purple pulp all the way to the bone.</p>', '202202130435istockphoto.jpg', 1, '2022-02-12 22:35:40', '2022-02-12 22:35:40'),
(7, 1, 'Blog-article convergence', '<span style=\"font-family: Montserrat, sans-serif; font-size: 16px; letter-spacing: 0.252px;\">Blog posts began to have more interviews. They presented interesting data. Posts got longer as bloggers sought to stand out and deliver more value, until 1,000 words has become fairly standard, and 2,000-word posts are not uncommon. SEO keywords’ value lessened as Google cracked down on keyword-stuffed content. Also, as blogs got more professional, many hired editors.</span>', '<p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 25px; font-size: 16px; line-height: 30px; text-rendering: optimizelegibility; font-family: Montserrat, sans-serif; letter-spacing: 0.252px; overflow-wrap: break-word;\">Blog posts started to get more and more like articles. As a bazillion blogs crowded the Internet, the bar began to raise.</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 25px; font-size: 16px; line-height: 30px; text-rendering: optimizelegibility; font-family: Montserrat, sans-serif; letter-spacing: 0.252px; overflow-wrap: break-word;\">Blog posts began to have more interviews. They presented interesting data. Posts got longer as bloggers sought to stand out and deliver more value, until 1,000 words has become fairly standard, and 2,000-word posts are not uncommon. SEO keywords’ value lessened as Google cracked down on keyword-stuffed content. Also, as blogs got more professional, many hired editors.</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 25px; font-size: 16px; line-height: 30px; text-rendering: optimizelegibility; font-family: Montserrat, sans-serif; letter-spacing: 0.252px; overflow-wrap: break-word;\">On the article-writing side, there was also movement. Many print magazines began posting copies of their articles online. Suddenly, magazine headlines needed to drive traffic, just like blog-post headlines, and headline styles evolved. They published more opinion-driven pieces from thought leaders. Some also put up blogs where they let writers hit the ‘publish’ button on their own.</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 25px; font-size: 16px; line-height: 30px; text-rendering: optimizelegibility; font-family: Montserrat, sans-serif; letter-spacing: 0.252px; overflow-wrap: break-word;\">Wordcounts shortened for print, as ad revenue migrated online. Some magazines went online-only. Their style got breezier and more casual.</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 25px; font-size: 16px; line-height: 30px; text-rendering: optimizelegibility; font-family: Montserrat, sans-serif; letter-spacing: 0.252px; overflow-wrap: break-word;\">To sum up, the two types of writing began to merge into one. Definitions got squishy, and now there’s a lot of confusion.</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 25px; font-size: 16px; line-height: 30px; text-rendering: optimizelegibility; font-family: Montserrat, sans-serif; letter-spacing: 0.252px; overflow-wrap: break-word;\">Except about one thing: Blog posts tend to pay crap, and articles tend to pay better.</p>', '202202130456blog-recent-2.jpg', 1, '2022-02-12 22:56:40', '2022-02-12 22:56:40');

-- --------------------------------------------------------

--
-- Table structure for table `ourteams`
--

CREATE TABLE `ourteams` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `twitter` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ourteams`
--

INSERT INTO `ourteams` (`id`, `name`, `designation`, `description`, `image`, `twitter`, `facebook`, `instagram`, `linkedin`, `created_at`, `updated_at`) VALUES
(1, 'Walter White', 'Chief Executive Office', '<span style=\"color: rgb(77, 70, 67); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: center; background-color: rgb(247, 247, 247);\">Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</span>', '202202120729team-1.jpg', 'https://twitter.com', 'https://facebook.com', 'https://instagram.com', 'https://www.linkedin.com', '2022-02-12 01:29:05', '2022-02-12 02:24:27'),
(2, 'Sarah Jhonson', 'Product Manager', '<span style=\"color: rgb(77, 70, 67); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: center; background-color: rgb(247, 247, 247);\">Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</span>', '202202131025blog-author.jpg', 'https://twitter.com', 'https://facebook.com', 'https://instagram.com', 'https://www.linkedin.com', '2022-02-12 01:31:10', '2022-02-13 04:25:30'),
(3, 'William Anderson', 'CTO', '<span style=\"color: rgb(77, 70, 67); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: center; background-color: rgb(247, 247, 247);\">Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</span>', '202202120731team-3.jpg', 'https://twitter.com', 'https://facebook.com', 'https://instagram.com', 'https://www.linkedin.com', '2022-02-12 01:31:46', '2022-02-12 02:24:14'),
(4, 'Amanda Jepson', 'Accountant', '<span style=\"color: rgb(77, 70, 67); font-family: &quot;Open Sans&quot;, sans-serif; font-size: 16px; text-align: center; background-color: rgb(247, 247, 247);\">Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</span>', '202202120732team-4.jpg', 'https://twitter.com', 'https://facebook.com', 'https://instagram.com', 'https://www.linkedin.com', '2022-02-12 01:32:26', '2022-02-12 02:24:07');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1=Active, 0=Inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `user_id`, `category_id`, `name`, `description`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 4, 'Black Olives', '<span style=\"font-family: &quot;Work Sans&quot;, Arial, sans-serif; font-size: 17px; letter-spacing: -0.1px;\">In addition to the difference in their state of ripeness, there are other distinguishing characteristics when it comes to packaged green and black olives. Bottled green olives are usually pitted, and often stuffed with various fillings, including&nbsp;</span><a href=\"https://www.thespruceeats.com/all-about-pimento-995739\" data-component=\"link\" data-source=\"inlineLink\" data-type=\"internalLink\" data-ordinal=\"1\" rel=\"noopener noreferrer\" style=\"box-sizing: border-box; background-image: linear-gradient(to right, rgb(0, 143, 185) 0px, rgb(0, 143, 185) 100%); background-position: 0px 97%; background-repeat: repeat-x; background-size: 100% 1px; color: rgb(0, 143, 185); transition: background-image 0.25s ease 0s, color 0.25s ease 0s; font-family: &quot;Work Sans&quot;, Arial, sans-serif; font-size: 17px; letter-spacing: -0.1px; background-color: rgb(255, 255, 255);\">pimentos</a><span style=\"font-family: &quot;Work Sans&quot;, Arial, sans-serif; font-size: 17px; letter-spacing: -0.1px;\">,&nbsp;</span><a href=\"https://www.thespruceeats.com/almond-fact-sheet-1807487\" data-component=\"link\" data-source=\"inlineLink\" data-type=\"internalLink\" data-ordinal=\"2\" rel=\"noopener noreferrer\" style=\"box-sizing: border-box; background-image: linear-gradient(to right, rgb(0, 143, 185) 0px, rgb(0, 143, 185) 100%); background-position: 0px 97%; background-repeat: repeat-x; background-size: 100% 1px; color: rgb(0, 143, 185); transition: background-image 0.25s ease 0s, color 0.25s ease 0s; font-family: &quot;Work Sans&quot;, Arial, sans-serif; font-size: 17px; letter-spacing: -0.1px; background-color: rgb(255, 255, 255);\">almonds</a><span style=\"font-family: &quot;Work Sans&quot;, Arial, sans-serif; font-size: 17px; letter-spacing: -0.1px;\">,&nbsp;</span><a href=\"https://www.thespruceeats.com/what-are-anchovies-1806993\" data-component=\"link\" data-source=\"inlineLink\" data-type=\"internalLink\" data-ordinal=\"3\" rel=\"noopener noreferrer\" style=\"box-sizing: border-box; background-image: linear-gradient(to right, rgb(0, 143, 185) 0px, rgb(0, 143, 185) 100%); background-position: 0px 97%; background-repeat: repeat-x; background-size: 100% 1px; color: rgb(0, 143, 185); transition: background-image 0.25s ease 0s, color 0.25s ease 0s; font-family: &quot;Work Sans&quot;, Arial, sans-serif; font-size: 17px; letter-spacing: -0.1px; background-color: rgb(255, 255, 255);\">anchovies</a><span style=\"font-family: &quot;Work Sans&quot;, Arial, sans-serif; font-size: 17px; letter-spacing: -0.1px;\">, jalapenos, onions, or&nbsp;</span><a href=\"https://www.thespruceeats.com/what-are-capers-1807002\" data-component=\"link\" data-source=\"inlineLink\" data-type=\"internalLink\" data-ordinal=\"4\" rel=\"noopener noreferrer\" style=\"box-sizing: border-box; background-image: linear-gradient(to right, rgb(0, 143, 185) 0px, rgb(0, 143, 185) 100%); background-position: 0px 97%; background-repeat: repeat-x; background-size: 100% 1px; color: rgb(0, 143, 185); transition: background-image 0.25s ease 0s, color 0.25s ease 0s; font-family: &quot;Work Sans&quot;, Arial, sans-serif; font-size: 17px; letter-spacing: -0.1px; background-color: rgb(255, 255, 255);\">capers</a><span style=\"font-family: &quot;Work Sans&quot;, Arial, sans-serif; font-size: 17px; letter-spacing: -0.1px;\">. Canned black olives are graded</span>', '2022021409493.1.1.MAS_TARRES_AOVE_DOPSiurana_Arbequina.jpg', 1, '2022-02-09 04:28:02', '2022-02-14 03:49:00'),
(2, 1, 7, 'Cured Olives', '<span style=\"font-family: &quot;Work Sans&quot;, Arial, sans-serif; font-size: 17px; letter-spacing: -0.1px;\">Depending on which method is used, curing olives can take from a few days to a few months. Lye curing has the shortest fermentation time while brining takes the longest.&nbsp;</span><a href=\"https://www.thespruceeats.com/lye-cured-green-olives-recipe-1807881\" data-component=\"link\" data-source=\"inlineLink\" data-type=\"internalLink\" data-ordinal=\"1\" rel=\"noopener noreferrer\" style=\"box-sizing: border-box; background-image: linear-gradient(to right, rgb(0, 143, 185) 0px, rgb(0, 143, 185) 100%); background-position: 0px 97%; background-repeat: repeat-x; background-size: 100% 1px; color: rgb(0, 143, 185); transition: background-image 0.25s ease 0s, color 0.25s ease 0s; font-family: &quot;Work Sans&quot;, Arial, sans-serif; font-size: 17px; letter-spacing: -0.1px; background-color: rgb(255, 255, 255);\">Green olives</a><span style=\"font-family: &quot;Work Sans&quot;, Arial, sans-serif; font-size: 17px; letter-spacing: -0.1px;\">&nbsp;must be soaked in a lye solution before brining, whereas ripe black olives can proceed straight to brining. The longer the olive is permitted to ferment in its own brine, the less bitter and more intricate its flavor will become.</span>', '202202091033images.jfif', 1, '2022-02-09 04:33:30', '2022-02-09 04:33:30'),
(3, 1, 5, 'Green Olive Varieties', '<span style=\"font-family: &quot;Work Sans&quot;, Arial, sans-serif; font-size: 17px; letter-spacing: -0.1px;\">There are many types of olives, but you may come across just a few in your local supermarket or gourmet grocer. The Manzanilla is a Spanish green olive that is lightly lye-cured then packed in salt and lactic acid brine. These olives are most often available pitted and sometimes stuffed. Another Spanish olive is the Gordal, which means \"fat one,\" a fitting name for this plump, rounded green olive. They are meaty and rich tasting, and often served as a tapas.</span>', '202202091034gettyimages-.jpg', 1, '2022-02-09 04:34:15', '2022-02-09 04:34:15'),
(4, 1, 6, 'Black Olive Varieties', '<span style=\"font-family: &quot;Work Sans&quot;, Arial, sans-serif; font-size: 17px; letter-spacing: -0.1px;\">Black olives aren\'t just plain and found in a can; there are plenty of interesting cured black olives to choose from, such as Italian olives. One is the Liguria, a black olive that is salt-brine cured with a vibrant flavor, and sometimes packed with their stems. Another Italian black olive is the Ponentine, salt-brine cured and then packed in vinegar; it has a mild flavor. The Gaeta is also a ripe olive; it is dry-salt cured and then rubbed with oil. It has a wrinkled appearance, mild flavor, and is often packed with rosemary and other herbs. And one black Italian olive that is popular at tastings is the Lugano; it is usually very salty and sometimes packed with olive leaves.</span>', '202202130838IMG_3383.jpg', 1, '2022-02-09 04:35:04', '2022-02-13 02:38:30'),
(5, 1, 8, 'Storing Olives', '<span style=\"font-family: &quot;Work Sans&quot;, Arial, sans-serif; font-size: 17px; letter-spacing: -0.1px;\">Once you bring home these delicious olives, you need to make sure you store them properly to maintain their freshness and flavor. Unopened olives can be stored at room temperature up to 2 years. Opened olives (including those from an olive bar) should be refrigerated in their own liquid in a non-metal container and will last for 1 to 2 months after opening.</span>', '202202091035Oliven_V1.jpg', 1, '2022-02-09 04:35:40', '2022-02-09 04:35:40'),
(6, 1, 4, 'How to Brine and Cure Your Own Olives', '<span style=\"font-family: &quot;Work Sans&quot;, Arial, sans-serif; font-size: 17px; letter-spacing: -0.1px;\">If you\'re lucky enough to have olive trees, you may have considered eating one of the fruits straight from the branch only to discover that there\'s a big difference between the olive on a tree and the olive on your plate. That\'s because the olives we enjoy are essentially&nbsp;</span><a href=\"https://www.thespruceeats.com/pickling-4162193\" data-component=\"link\" data-source=\"inlineLink\" data-type=\"internalLink\" data-ordinal=\"1\" style=\"box-sizing: border-box; background-image: linear-gradient(to right, rgb(0, 143, 185) 0px, rgb(0, 143, 185) 100%); background-position: 0px 97%; background-repeat: repeat-x; background-size: 100% 1px; color: rgb(0, 143, 185); transition: background-image 0.25s ease 0s, color 0.25s ease 0s; font-family: &quot;Work Sans&quot;, Arial, sans-serif; font-size: 17px; letter-spacing: -0.1px; background-color: rgb(255, 255, 255);\">pickles</a><span style=\"font-family: &quot;Work Sans&quot;, Arial, sans-serif; font-size: 17px; letter-spacing: -0.1px;\">. Whether steeped in oil or a salt brine, olives only become truly edible after curing. The raw fruit is bursting with&nbsp;oleuropein, a bitter compound that must be removed prior to eating.</span>', '202202091105GettyImages-81740527web-58a47d185f9b58819c9daf36.jpg', 1, '2022-02-09 05:05:09', '2022-02-09 05:05:09'),
(7, 1, 5, 'Olive Varieties and Types', '<p id=\"mntl-sc-block_1-0\" class=\"comp mntl-sc-block mntl-sc-block-html\" style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 1rem; margin-left: 0px; padding: 0px; letter-spacing: -0.1px; counter-reset: section 0; font-family: &quot;Work Sans&quot;, Arial, sans-serif; font-size: 17px;\">Olives are one of those foods that can range from simple to complex—from a basic black to a vibrant green, from very mild to super salty. They can be eaten alone out of hand or incorporated into recipes. And in many cases, you either love or hate this small-sized fruit, or you may prefer a particular variety.</p><div id=\"mntl-sc-block_1-0-1\" class=\"comp mntl-sc-block mntl-sc-block-adslot mntl-block\" style=\"box-sizing: border-box; margin: 0px; padding: 0px; font-family: &quot;Work Sans&quot;, Arial, sans-serif; font-size: 17px;\"><div id=\"ad-group-1_1-0\" class=\"comp ad-group-1 mntl-block\" style=\"box-sizing: border-box; margin: 0px; padding: 0px;\"><div id=\"billboard1-sticky-dynamic_1-0\" class=\"comp scads-to-load right-rail__item billboard-sticky billboard1-sticky-dynamic billboard-sticky--sc mntl-sc-sticky-billboard scads-stick-in-parent scads-ad-placed\" data-height=\"1050\" style=\"box-sizing: border-box; margin: 0px auto; padding: 0px; height: 1050px; position: absolute; width: 300px; right: -20rem; min-width: 300px; top: -11px; float: none;\"><div id=\"mntl-sc-sticky-billboard-ad_3-0\" class=\"comp mntl-billboard mntl-sc-sticky-billboard-ad billboard1-dynamic mntl-dynamic-billboard mntl-gpt-dynamic-adunit mntl-gpt-adunit gpt billboard dynamic js-immediate-ad\" data-ad-width=\"300\" data-ad-height=\"250\" style=\"box-sizing: border-box; margin: 0px; padding: 0px; text-align: center; min-width: 300px; min-height: 270px; transform: translateY(780px);\"><div id=\"billboard\" class=\"wrapper\" data-type=\"billboard\" data-pos=\"atf\" data-priority=\"2\" data-sizes=\"[[300, 250],[300, 600],[300, 1050],[1, 2],&quot;fluid&quot;]\" data-rtb=\"true\" data-targeting=\"{}\" data-auction-floor-id=\"24518a235b5b4850914f0fec03efc226\" data-auction-floor-value=\"5\" data-google-query-id=\"CKbPx8mx8vUCFY2eSwUdeOsHVA\" style=\"box-sizing: border-box; margin: 0px auto; padding: 0px; min-height: 270px; min-width: 300px; width: 300px;\"><div id=\"google_ads_iframe_/479/thespruceeats/spreats_ingredients/billboard_0__container__\" style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0pt none; display: inline-block; width: 300px; height: 250px;\"><iframe frameborder=\"0\" src=\"https://3cc3bafead0d6838559675b1462fb340.safeframe.googlesyndication.com/safeframe/1-0-38/html/container.html\" id=\"google_ads_iframe_/479/thespruceeats/spreats_ingredients/billboard_0\" title=\"3rd party ad content\" name=\"\" scrolling=\"no\" marginwidth=\"0\" marginheight=\"0\" width=\"300\" height=\"250\" data-is-safeframe=\"true\" sandbox=\"allow-forms allow-popups allow-popups-to-escape-sandbox allow-same-origin allow-scripts allow-top-navigation-by-user-activation\" role=\"region\" aria-label=\"Advertisement\" tabindex=\"0\" data-google-container-id=\"3\" data-load-complete=\"true\" style=\"box-sizing: border-box; margin: 0px; padding: 0px; border-width: 0px; border-style: initial; vertical-align: bottom;\"></iframe></div></div></div></div></div></div><p id=\"mntl-sc-block_1-0-2\" class=\"comp mntl-sc-block mntl-sc-block-html\" style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 1rem; margin-left: 0px; padding: 0px; letter-spacing: -0.1px; counter-reset: section 0; font-family: &quot;Work Sans&quot;, Arial, sans-serif; font-size: 17px;\">It may surprise you to learn that the only difference between green olives and black olives is ripeness; unripe olives are green, whereas fully ripe olives are black. Because the raw green olives have a naturally bitter taste, they need to be&nbsp;<a href=\"https://www.thespruceeats.com/brining-and-curing-olives-1808582\" data-component=\"link\" data-source=\"inlineLink\" data-type=\"internalLink\" data-ordinal=\"1\" rel=\"noopener noreferrer\" style=\"box-sizing: border-box; background-image: linear-gradient(to right, rgb(0, 143, 185) 0px, rgb(0, 143, 185) 100%); background-position: 0px 97%; background-repeat: repeat-x; background-size: 100% 1px; color: rgb(0, 143, 185); transition: background-image 0.25s ease 0s, color 0.25s ease 0s;\">cured</a>; this can be done through various methods including oil-curing, water-curing, brining, dry-curing, or placing in a lye solution. As olives are the Mediterranean in origin, most varieties come from Spain, Italy, Greece, and France.</p>', '202202091108GettyImages-74411680-57f94bf75f9b586c35770f22.jpg', 1, '2022-02-09 05:08:16', '2022-02-09 05:08:16'),
(8, 1, 5, 'Brine-Cured Olives', '<span style=\"font-family: &quot;Work Sans&quot;, Arial, sans-serif; font-size: 17px; letter-spacing: -0.1px;\">Brine-cured olives are an ideal salty nibble as well as an essential ingredient in a variety of dishes. They can be purchased, but they\'re often expensive. Making them at home is a simple process and requires nothing but time, water, vinegar, and salt. Whether you have an olive tree or can get access to fresh olives at a good price, this simple recipe makes flavorful salted olives to use in&nbsp;</span><a href=\"https://www.thespruceeats.com/moroccan-olives-with-harissa-recipe-2394937\" data-component=\"link\" data-source=\"inlineLink\" data-type=\"internalLink\" data-ordinal=\"1\" style=\"box-sizing: border-box; background-image: linear-gradient(to right, rgb(0, 143, 185) 0px, rgb(0, 143, 185) 100%); background-position: 0px 97%; background-repeat: repeat-x; background-size: 100% 1px; color: rgb(0, 143, 185); transition: background-image 0.25s ease 0s, color 0.25s ease 0s; font-family: &quot;Work Sans&quot;, Arial, sans-serif; font-size: 17px; letter-spacing: -0.1px; background-color: rgb(255, 255, 255);\">appetizers</a><span style=\"font-family: &quot;Work Sans&quot;, Arial, sans-serif; font-size: 17px; letter-spacing: -0.1px;\">,&nbsp;</span><a href=\"https://www.thespruceeats.com/mediterranean-chicken-salad-4685652\" data-component=\"link\" data-source=\"inlineLink\" data-type=\"internalLink\" data-ordinal=\"2\" style=\"box-sizing: border-box; background-image: linear-gradient(to right, rgb(0, 143, 185) 0px, rgb(0, 143, 185) 100%); background-position: 0px 97%; background-repeat: repeat-x; background-size: 100% 1px; color: rgb(0, 143, 185); transition: background-image 0.25s ease 0s, color 0.25s ease 0s; font-family: &quot;Work Sans&quot;, Arial, sans-serif; font-size: 17px; letter-spacing: -0.1px; background-color: rgb(255, 255, 255);\">salads</a><span style=\"font-family: &quot;Work Sans&quot;, Arial, sans-serif; font-size: 17px; letter-spacing: -0.1px;\">, or&nbsp;</span><a href=\"https://www.thespruceeats.com/pasta-capers-olives-pine-nuts-recipe-1808354\" data-component=\"link\" data-source=\"inlineLink\" data-type=\"internalLink\" data-ordinal=\"3\" style=\"box-sizing: border-box; background-image: linear-gradient(to right, rgb(0, 143, 185) 0px, rgb(0, 143, 185) 100%); background-position: 0px 97%; background-repeat: repeat-x; background-size: 100% 1px; color: rgb(0, 143, 185); transition: background-image 0.25s ease 0s, color 0.25s ease 0s; font-family: &quot;Work Sans&quot;, Arial, sans-serif; font-size: 17px; letter-spacing: -0.1px; background-color: rgb(255, 255, 255);\">pasta</a><span style=\"font-family: &quot;Work Sans&quot;, Arial, sans-serif; font-size: 17px; letter-spacing: -0.1px;\">&nbsp;dishes, among many other recipes in which olives shine. These olives are especially delicious when served alongside&nbsp;</span><a href=\"https://www.thespruceeats.com/dry-salt-cured-olives-1327914\" data-component=\"link\" data-source=\"inlineLink\" data-type=\"internalLink\" data-ordinal=\"4\" style=\"box-sizing: border-box; background-image: linear-gradient(to right, rgb(0, 143, 185) 0px, rgb(0, 143, 185) 100%); background-position: 0px 97%; background-repeat: repeat-x; background-size: 100% 1px; color: rgb(0, 143, 185); transition: background-image 0.25s ease 0s, color 0.25s ease 0s; font-family: &quot;Work Sans&quot;, Arial, sans-serif; font-size: 17px; letter-spacing: -0.1px; background-color: rgb(255, 255, 255);\">dry salt-cured olives</a><span style=\"font-family: &quot;Work Sans&quot;, Arial, sans-serif; font-size: 17px; letter-spacing: -0.1px;\">&nbsp;for a pleasant taste and texture contrast.</span>', '20220209111072800444-56a30e0b5f9b58b7d0d0326e.jpg', 1, '2022-02-09 05:10:21', '2022-02-09 05:10:21');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED DEFAULT NULL,
  `sub_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `product_id`, `sub_image`, `created_at`, `updated_at`) VALUES
(4, 2, '202202091033images.jfif', '2022-02-09 04:33:30', '2022-02-09 04:33:30'),
(5, 2, '202202091033istockphoto.jpg', '2022-02-09 04:33:30', '2022-02-09 04:33:30'),
(6, 2, '202202091033istockphoto-12.jpg', '2022-02-09 04:33:30', '2022-02-09 04:33:30'),
(7, 3, '202202091034gettyimages-.jpg', '2022-02-09 04:34:15', '2022-02-09 04:34:15'),
(8, 3, '202202091034images.jfif', '2022-02-09 04:34:15', '2022-02-09 04:34:15'),
(9, 3, '202202091034istockphoto-6433.jpg', '2022-02-09 04:34:15', '2022-02-09 04:34:15'),
(10, 4, '202202091035Oliveoi.jpg', '2022-02-09 04:35:04', '2022-02-09 04:35:04'),
(11, 4, '202202091035olive-oil-500x500.jpg', '2022-02-09 04:35:04', '2022-02-09 04:35:04'),
(12, 4, '202202091035t5_2_.jpg', '2022-02-09 04:35:04', '2022-02-09 04:35:04'),
(13, 5, '202202091035Lucy-Oliva-Olive-Oil-150ML.jpg', '2022-02-09 04:35:40', '2022-02-09 04:35:40'),
(14, 5, '202202091035Oliven_V1.jpg', '2022-02-09 04:35:40', '2022-02-09 04:35:40'),
(15, 5, '202202091035olive-oil-in-a-.jpg', '2022-02-09 04:35:40', '2022-02-09 04:35:40'),
(16, 6, '202202091105Easy-Recipe-Homemade-Olives.jfif', '2022-02-09 05:05:09', '2022-02-09 05:05:09'),
(17, 6, '202202091105GettyImages-81740527web-58a47d185f9b58819c9daf36.jpg', '2022-02-09 05:05:09', '2022-02-09 05:05:09'),
(18, 6, '202202091105IMG_3383.jpg', '2022-02-09 05:05:09', '2022-02-09 05:05:09'),
(19, 7, '2022020911087dc8aa8b10ed5cbd938b49852b5ae6b5.jpg', '2022-02-09 05:08:16', '2022-02-09 05:08:16'),
(20, 7, '202202091108GettyImages-74411680-57f94bf75f9b586c35770f22.jpg', '2022-02-09 05:08:16', '2022-02-09 05:08:16'),
(21, 7, '202202091108Screen+Shot+2021-08-17+at+6.09.39+PM.png', '2022-02-09 05:08:16', '2022-02-09 05:08:16'),
(22, 8, '20220209111072800444-56a30e0b5f9b58b7d0d0326e.jpg', '2022-02-09 05:10:21', '2022-02-09 05:10:21'),
(23, 8, '202202091110GettyImages-74411680-57f94bf75f9b586c35770f22.jpg', '2022-02-09 05:10:21', '2022-02-09 05:10:21'),
(24, 8, '202202091110greenolives-5a85e5dfa18d9e0037a56ce5.jpg', '2022-02-09 05:10:21', '2022-02-09 05:10:21'),
(31, 1, '2022021410153.1.1.MAS_TARRES_AOVE_DOPSiurana_Arbequina.jpg', '2022-02-14 04:15:08', '2022-02-14 04:15:08'),
(32, 1, '20220214101523835-0w600h60.jpg', '2022-02-14 04:15:08', '2022-02-14 04:15:08'),
(33, 1, '202202141015Aceitunas-525x360-1.jpg', '2022-02-14 04:15:08', '2022-02-14 04:15:08');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `phone` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `map` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `phone`, `email`, `address`, `map`, `created_at`, `updated_at`) VALUES
(3, '01723344556', 'greenlewaf@gmail.com', 'A108 Adam Street New York, NY 535022 United States', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3650.3280951562974!2d90.3665091144569!3d23.80692939253257!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c0d6f6b8c2ff%3A0x3b138861ee9c8c30!2sMirpur%2010%20Roundabout%2C%20Dhaka%201216!5e0!3m2!1sen!2sbd!4v1644644200607!5m2!1sen!2sbd', '2022-02-11 23:37:13', '2022-02-11 23:53:24');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `title`, `description`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Welcome to Company', '<p>Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus et tempore modi architecto.</p>', '202202130721olive-slider.jpg', '2022-02-09 05:45:31', '2022-02-13 01:21:10'),
(2, 'Lorem Ipsum Dolor', 'Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus et tempore modi architecto', '202202130720OliveSlider.jpg', '2022-02-09 05:56:55', '2022-02-13 01:20:57'),
(3, 'Welcome to Green Product', 'Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus et tempore modi architecto', '202202130720Oliveoil-slider.jpg', '2022-02-09 06:02:14', '2022-02-13 01:20:40');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `saved_by` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `phone`, `username`, `email_verified_at`, `password`, `image`, `status`, `saved_by`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Mr. Admin', 'admin@gmail.com', '01723344556', NULL, NULL, '$2y$10$2ZqPLV15JcsU1w1bMdytyu66v.N2x/BxBv25nRDlcmTRJrs91i0Su', '202202131033userimg.png', 1, NULL, NULL, '2022-02-08 04:52:51', '2022-02-13 04:33:32');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_us`
--
ALTER TABLE `about_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `homepages`
--
ALTER TABLE `homepages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `image_galleries`
--
ALTER TABLE `image_galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newwsevents`
--
ALTER TABLE `newwsevents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ourteams`
--
ALTER TABLE `ourteams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_us`
--
ALTER TABLE `about_us`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `homepages`
--
ALTER TABLE `homepages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `image_galleries`
--
ALTER TABLE `image_galleries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `newwsevents`
--
ALTER TABLE `newwsevents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `ourteams`
--
ALTER TABLE `ourteams`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
