@extends('backend.layouts.admin_master')

@section('photogallery')
active
@endsection()

@section('admin_content')

<nav class="breadcrumb sl-breadcrumb">
    <a class="breadcrumb-item" href="{{ route('home') }}">GreenLeaf</a>
    <span class="breadcrumb-item active">Photo Gallery</span>
</nav>

<div class="sl-pagebody">
    <div class="row row-sm">
        <div class="col-md-12">
            <div class="card pd-20 pd-sm-40">
                <h6 class="card-body-title">
                    <i class="fa fa-list"></i> Photo Gallery List
                    <a href="{{ route('admin.photo.create') }}" class="btn btn-success btn-sm pull-right"><i class="fa fa-plus"></i> Add Photo Gallery</a>
                </h6>
                

                <div class="table-wrapper">
                    <table id="datatable1" class="table display responsive nowrap">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Image</th>
                                <th>Title</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($imagegalleries as $imagegallery)
                            <tr class="{{ $imagegallery->id }}">
                                <td>{{ $loop->index + 1 }}</td>
                                <td><img src="{{ asset('backend/images/galleries/'.$imagegallery->image) }}" width="50" height="50" alt=""></td>
                                <td>{{ $imagegallery->title }}</td>
                                <td>
                                    <a href="{{ route('admin.photo.edit',$imagegallery->id) }}" class="btn btn-success btn-sm"><i class="fa fa-edit"></i></a>
                                    <a href="{{ route('admin.photo.delete') }}" id="delete" data-token="{{csrf_token()}}" data-id="{{$imagegallery->id}}" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div><!-- table-wrapper -->
            </div><!-- card -->
        </div>

    </div><!-- row -->
</div><!-- sl-pagebody -->
@endsection
