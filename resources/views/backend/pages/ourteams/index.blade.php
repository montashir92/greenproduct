@extends('backend.layouts.admin_master')

@section('team')
active
@endsection()

@section('admin_content')

<nav class="breadcrumb sl-breadcrumb">
    <a class="breadcrumb-item" href="{{ route('home') }}">GreenLeaf</a>
    <span class="breadcrumb-item active">Ourteam</span>
</nav>

<div class="sl-pagebody">
    <div class="row row-sm">
        <div class="col-md-12">
            <div class="card pd-20 pd-sm-40">
                <h6 class="card-body-title">
                    <i class="fa fa-list"></i> Ourteam List
                    <a href="{{ route('admin.ourteam.create') }}" class="btn btn-success btn-sm pull-right"><i class="fa fa-plus"></i> Add Ourteam</a>
                </h6>
                

                <div class="table-wrapper">
                    <table id="datatable1" class="table display responsive nowrap">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Image</th>
                                <th>Name</th>
                                <th>Designation</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($ourteams as $ourteam)
                            <tr class="{{ $ourteam->id }}">
                                <td>{{ $loop->index + 1 }}</td>
                                <td><img src="{{ asset('backend/images/teams/'.$ourteam->image) }}" alt="" width="50" height="50"></td>
                                <td>{{ $ourteam->name }}</td>
                                <td>{{ $ourteam->designation }}</td>
                                <td>
                                    <a href="{{ route('admin.ourteam.edit',$ourteam->id) }}" class="btn btn-success btn-sm"><i class="fa fa-edit"></i></a>
                                    <a href="{{ route('admin.ourteam.delete') }}" id="delete" data-token="{{csrf_token()}}" data-id="{{$ourteam->id}}" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div><!-- table-wrapper -->
            </div><!-- card -->
        </div>

    </div><!-- row -->
</div><!-- sl-pagebody -->
@endsection
