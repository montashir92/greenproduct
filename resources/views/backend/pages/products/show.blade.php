@extends('backend.layouts.admin_master')

@section('product')
active
@endsection()

@section('admin_content')

<nav class="breadcrumb sl-breadcrumb">
    <a class="breadcrumb-item" href="{{ route('home') }}">GreenLeaf</a>
    <span class="breadcrumb-item active">Product</span>
</nav>

<div class="sl-pagebody">
    <div class="row row-sm">
        <div class="col-md-12">
            <div class="card pd-20 pd-sm-40">
                <h6 class="card-body-title">
                    <i class="fa fa-info-circle"></i> Product Details
                    <a href="{{ route('admin.products') }}" class="btn btn-success btn-sm pull-right"><i class="fa fa-list"></i> All Product</a>
                </h6>
                

                <div class="table-wrapper">
                    <table id="datatable1" class="table table-bordered table-striped">
                        <tr>
                            <th width="30%">Product Name</th>
                            <td>{{ $product->name }}</td>
                        </tr>
                        
                        <tr>
                            <th width="30%">Category Name</th>
                            <td>{{ $product->category->name }}</td>
                        </tr>
                        
                        <tr>
                            <th width="30%">Description</th>
                            <td>{!! $product->description !!}</td>
                        </tr>
                        
                        <tr>
                            <th width="30%">Status</th>
                            <td>
                                @if($product->status == 1)
                                    <span class='badge badge-success'>Active</span>
                                @else
                                    <span class='badge badge-warning'>Active</span>
                                @endif
                            </td>
                        </tr>
                        
                        <tr>
                            <th width="30%">Image</th>
                            <td><img src="{{ asset('backend/images/products/'.$product->image) }}" width="100" height="100" alt=""></td>
                        </tr>
                        
                        <tr>
                            <th width="30%">Image Gallery</th>
                            <td>
                                @foreach($multiimages as $multiima)
                                    <img src="{{ asset('backend/images/sub_products/'.$multiima->sub_image) }}" width="100" height="100" alt="">
                                @endforeach
                            </td>
                        </tr>
                    </table>
                </div><!-- table-wrapper -->
            </div><!-- card -->
        </div>

    </div><!-- row -->
</div><!-- sl-pagebody -->
@endsection
