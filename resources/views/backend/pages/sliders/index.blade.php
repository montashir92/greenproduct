@extends('backend.layouts.admin_master')

@section('slider')
active
@endsection()

@section('admin_content')

<nav class="breadcrumb sl-breadcrumb">
    <a class="breadcrumb-item" href="{{ route('home') }}">GreenLeaf</a>
    <span class="breadcrumb-item active">Slider</span>
</nav>

<div class="sl-pagebody">
    <div class="row row-sm">
        <div class="col-md-12">
            <div class="card pd-20 pd-sm-40">
                <h6 class="card-body-title">
                    <i class="fa fa-list"></i> Slider List
                    <a href="{{ route('admin.slider.create') }}" class="btn btn-success btn-sm pull-right"><i class="fa fa-plus"></i> Add Slider</a>
                </h6>
                

                <div class="table-wrapper">
                    <table id="datatable1" class="table display responsive nowrap">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Image</th>
                                <th>Title</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($sliders as $slider)
                            <tr class="{{ $slider->id }}">
                                <td>{{ $loop->index + 1 }}</td>
                                <td><img src="{{ asset('backend/images/sliders/'.$slider->image) }}" width="50" alt=''></td>
                                <td>{{ $slider->title }}</td>
                                <td>
                                    <a href="{{ route('admin.slider.edit',$slider->id) }}" class="btn btn-success btn-sm"><i class="fa fa-edit"></i></a>
                                    <a href="{{ route('admin.slider.delete') }}" id="delete" data-token="{{csrf_token()}}" data-id="{{$slider->id}}" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div><!-- table-wrapper -->
            </div><!-- card -->
        </div>

    </div><!-- row -->
</div><!-- sl-pagebody -->
@endsection
