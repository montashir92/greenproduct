<div class="sl-logo"><a href="{{ route('home') }}"><i class="icon ion-android-star-outline"></i> GreenLeaf</a></div>
<div class="sl-sideleft">
    
    <div class="sl-sideleft-menu">
        <a href="{{ route('home') }}" class="sl-menu-link {{ Route::is('home') ? 'active' : '' }}">
            <div class="sl-menu-item">
                <i class="menu-item-icon icon ion-ios-home-outline tx-22"></i>
                <span class="menu-item-label">Dashboard</span>
            </div><!-- menu-item -->
        </a><!-- sl-menu-link -->
        
        <a href="{{ route('admin.products') }}" class="sl-menu-link @yield('product')">
            <div class="sl-menu-item">
                <i class="menu-item-icon ion-ios-pie-outline tx-20"></i>
                <span class="menu-item-label">Products</span>
            </div><!-- menu-item -->
        </a><!-- sl-menu-link -->
        <a href="{{ route('admin.categories') }}" class="sl-menu-link @yield('category')">
            <div class="sl-menu-item">
                <i class="menu-item-icon icon ion-ios-photos-outline tx-20"></i>
                <span class="menu-item-label">Category</span>
            </div><!-- menu-item -->
        </a><!-- sl-menu-link -->
        
        <a href="{{ route('admin.sliders') }}" class="sl-menu-link @yield('slider')">
            <div class="sl-menu-item">
                <i class="menu-item-icon icon ion-ios-gear-outline tx-24"></i>
                <span class="menu-item-label">Sliders</span>
            </div><!-- menu-item -->
        </a><!-- sl-menu-link -->
        
        <a href="{{ route('admin.abouts') }}" class="sl-menu-link @yield('about')">
            <div class="sl-menu-item">
                <i class="menu-item-icon icon ion-ios-filing-outline tx-24"></i>
                <span class="menu-item-label">About Us</span>
            </div><!-- menu-item -->
        </a><!-- sl-menu-link -->
       
        <a href="{{ route('admin.ourteams') }}" class="sl-menu-link @yield('team')">
            <div class="sl-menu-item">
                <i class="menu-item-icon icon ion-ios-paper-outline tx-22"></i>
                <span class="menu-item-label">Our Team</span>
            </div><!-- menu-item -->
        </a><!-- sl-menu-link -->
        
        <a href="{{ route('admin.photos') }}" class="sl-menu-link @yield('photogallery')">
            <div class="sl-menu-item">
                <i class="menu-item-icon icon ion-ios-photos-outline tx-22"></i>
                <span class="menu-item-label">Photo Gallery</span>
            </div><!-- menu-item -->
        </a><!-- sl-menu-link -->
        
        <a href="{{ route('admin.newsevents') }}" class="sl-menu-link @yield('newsevent')">
            <div class="sl-menu-item">
                <i class="menu-item-icon icon ion-ios-body-outline tx-22"></i>
                <span class="menu-item-label">News & Event</span>
            </div><!-- menu-item -->
        </a><!-- sl-menu-link -->
        
        <a href="{{ route('admin.clients') }}" class="sl-menu-link @yield('client')">
            <div class="sl-menu-item">
                <i class="menu-item-icon icon ion-ios-cog-outline tx-22"></i>
                <span class="menu-item-label">Client</span>
            </div><!-- menu-item -->
        </a><!-- sl-menu-link -->
        <a href="{{ route('admin.contacts') }}" class="sl-menu-link @yield('contact')">
            <div class="sl-menu-item">
                <i class="menu-item-icon icon ion-ios-navigate-outline tx-24"></i>
                <span class="menu-item-label">Contact</span>
            </div><!-- menu-item -->
        </a>
        <a href="{{ route('admin.settings') }}" class="sl-menu-link @yield('setting')">
            <div class="sl-menu-item">
                <i class="menu-item-icon icon ion-ios-settings tx-20"></i>
                <span class="menu-item-label">Setting</span>
            </div><!-- menu-item -->
        </a><!-- sl-menu-link -->
    </div><!-- sl-sideleft-menu -->

    <br>
</div><!-- sl-sideleft -->