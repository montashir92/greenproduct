@extends('frontend.layouts.master')
@section('content')

@section('title') GreenLeaf - About Us @endsection

<main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
        <div class="container">

            <div class="d-flex justify-content-between align-items-center">
                <h2>About</h2>
                <ol>
                    <li><a href="{{ route('index') }}">Home</a></li>
                    <li>About</li>
                </ol>
            </div>

        </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= About Us Section ======= -->
    <section id="about-us" class="about-us">
        <div class="container" data-aos="fade-up">

            <div class="row content">
                <div class="col-lg-6 about_img" data-aos="fade-right">
                    <h2>{{ $about->title }}</h2>
                    <img src="{{ asset('backend/images/abouts/'.$about->image) }}" alt="">
                </div>
                <div class="col-lg-6 pt-4 pt-lg-0" data-aos="fade-left">
                    <p>
                        {!! $about->details !!}
                    </p>
                </div>
            </div>

        </div>
    </section><!-- End About Us Section -->

    <!-- ======= Our Team Section ======= -->
    @include('frontend.partials.teams')



</main><!-- End #main -->


@endsection