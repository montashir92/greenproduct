
@extends('frontend.layouts.master')

@section('title') GreenLeaf - Details News & Event @endsection

@section('content')


  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>News & Event Details</h2>
          <ol>
            <li><a href="{{ route('index') }}">Home</a></li>
            <li>Single</li>
          </ol>
        </div>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Blog Section ======= -->
    <section id="blog" class="blog">
      <div class="container" data-aos="fade-up">

        <div class="row">

          <div class="col-lg-8 entries">
             
            <article class="blogentry">

              <div class="entry-img">
                <img src="{{ asset('backend/images/newsevent/'.$blog->image) }}" width="100%" alt="" class="img-fluid">
              </div>

              <h2 class="entry-title">
                <a href="">{{ $blog->title }}</a>
              </h2>

              <div class="entry-meta">
                
              </div>

              <div class="entry-content">
                <p>
                  {!! $blog->long_desc !!}
                </p>
                
              </div>

            </article><!-- End blog entry -->

          </div><!-- End blog entries list -->

          <div class="col-lg-4">

            <div class="sidebar">

              <h3 class="sidebar-title">Recent Posts</h3>
              <div class="sidebar-item recent-posts">
                @foreach($recent_posts as $recent_post)
                <div class="post-item clearfix">
                  <img src="{{ asset('backend/images/newsevent/'.$recent_post->image) }}" alt="">
                  <h4><a href="{{ route('blog.details', $recent_post->id) }}">{{ $recent_post->title }}</a></h4>
                  <time datetime="2020-01-01">{{ date('F j Y',strtotime($recent_post->created_at)) }}</time>
                </div>
                @endforeach
              </div><!-- End sidebar recent posts-->

            </div><!-- End sidebar -->

          </div><!-- End blog sidebar -->

        </div>

      </div>
    </section><!-- End Blog Section -->

  </main><!-- End #main -->
  
@endsection
