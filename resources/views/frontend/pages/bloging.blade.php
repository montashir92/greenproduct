
@extends('frontend.layouts.master')

@section('title') GreenLeaf - News & Event @endsection

@section('content')

<style>
    nav svg{
        height: 20px;
    }
    nav .hidden{
        display: block !important;
    }
</style>


  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>News & Event</h2>
          <ol>
            <li><a href="{{ route('index') }}">Home</a></li>
            <li>News & Event</li>
          </ol>
        </div>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Blog Section ======= -->
    <section id="blog" class="blog">
      <div class="container" data-aos="fade-up">

        <div class="row">

         @foreach($blogs as $blog)
          <div class="col-md-6 entries">
              
            <article class="entry">

              <div class="entry-img">
                  <img src="{{ asset('backend/images/newsevent/'.$blog->image) }}" width="100%" alt="" class="img-fluid">
              </div>

              <h2 class="entry-title">
                <a href="{{ route('blog.details', $blog->id) }}">{{ $blog->title }}</a>
              </h2>

              <div class="entry-content">
                <p>
                  {!! $blog->short_desc  !!}
                </p>
                <div class="read-more">
                  <a href="{{ route('blog.details', $blog->id) }}">Read More</a>
                </div>
              </div>

            </article><!-- End blog entry -->
          </div><!-- End blog entries list -->
          @endforeach
        </div>
        
        <div class="blog-pagination justify-content-center">
            {{ $blogs->links() }}
        </div>
      </div>
        
    </section><!-- End Blog Section -->

  </main><!-- End #main -->
  
@endsection
