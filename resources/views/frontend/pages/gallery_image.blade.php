@extends('frontend.layouts.master')

@section('title') GreenLeaf - Gallery Image @endsection

@section('content')



<main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
        <div class="container">

            <div class="d-flex justify-content-between align-items-center">
                <h2>Image Gallery</h2>
                <ol>
                    <li><a href="{{ route('index') }}">Home</a></li>
                    <li>Image Gallery</li>
                </ol>
            </div>

        </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services section-bg">
        <div class="container" data-aos="fade-up">

            <div class="row">
                @foreach($photogalleries as $photogallery)
                <div class="col-lg-3 col-md-6 d-flex align-items-stretch mb-3" data-aos="zoom-in" data-aos-delay="100">
                    <a href="{{ asset('backend/images/galleries/'.$photogallery->image) }}" class="image-link">
                        <img src="{{ asset('backend/images/galleries/'.$photogallery->image) }}" height="100%" class="card-img-top" alt="...">
                    </a>
                </div>
                
                @endforeach

            </div>

        </div>
    </section><!-- End Services Section -->



</main><!-- End #main -->


@endsection

@section('scripts')
 <script>
    $(function(){
	$('.image-link').viewbox();
    });

    $(function(){
        $('.image-link').viewbox({
            setTitle: true,
            margin: 20,
            resizeDuration: 300,
            openDuration: 200,
            closeDuration: 200,
            closeButton: true,
            navButtons: true,
            closeOnSideClick: true,
            nextOnContentClick: true
        });
    });
 </script>
@endsection