@extends('frontend.layouts.master')
@section('content')

@section('title') GreenLeaf - Home @endsection


<!-- ======= Hero Section ======= -->
@include('frontend.partials.slider')

<main id="main">

    <!-- ======= About Us Section ======= -->
    <section id="about-us" class="about-us">
        <div class="container" data-aos="fade-up">

            <div class="row content">
                <div class="col-md-4 col-md-6 about_img" data-aos="fade-right">
                    <h2>{{ $about->title }}</h2>
                    <img src="{{ asset('backend/images/abouts/'.$about->image) }}" alt="">
                </div>
                <div class="col-lg-6 pt-4 pt-lg-0" data-aos="fade-left">
                    <p>
                        {!! $about->details !!}
                    </p>
                </div>
            </div>

        </div>
    </section><!-- End About Us Section -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services section-bg">
    <div class="container" data-aos="fade-up">

        <div class="row">
            <div class="section-title">
                <h2>New Arrivals</h2>
            </div>
            
            @foreach($new_products as $new_product)
            <div class="col-md-3" data-aos="zoom-in" data-aos-delay="100">
                <div class="card mb-4">
                    <a href="{{ route('product.details', $new_product->id) }}">
                        <img src="{{ asset('backend/images/products/'.$new_product->image) }}" height="250" width="100%" class="card-img-top" alt="...">
                    </a>
                
                <div class="card-body text-center">
                  <h5>{{ $new_product->name }}</h5>
                  <a href="{{ route('product.details', $new_product->id) }}" class="btn btn-primary btn-sm">View Details</a>
                </div>
              </div>
            </div>
            @endforeach

        </div>

    </div>
</section><!-- End Services Section -->

    <!-- ======= Portfolio Section ======= -->
    @include('frontend.partials.portfolio')
    
    <!-- ======= Our Clients Section ======= -->
    @include('frontend.partials.ourclient')

</main><!-- End #main -->
@endsection