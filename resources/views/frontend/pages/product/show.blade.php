@extends('frontend.layouts.master')
@section('content')


  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>Product Details</h2>
          <ol>
            <li><a href="">Home</a></li>
            <li><a href="">Product</a></li>
            <li>Product Details</li>
          </ol>
        </div>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Portfolio Details Section ======= -->
    <section id="portfolio-details" class="portfolio-details">
      <div class="container" data-aos="fade-up">

        <div class="row gy-4">

          <div class="col-lg-8">
            <div class="portfolio-details-slider swiper">
              <div class="swiper-wrapper align-items-center">
                <div class="swiper-slide">
                    <img src="{{ asset('backend/images/products/'.$product->image) }}" height="400" alt="">
                </div>
                @foreach($product_images as $product_image)
                <div class="swiper-slide">
                    <img src="{{ asset('backend/images/sub_products/'.$product_image->sub_image) }}" height="400" alt="">
                </div>
                @endforeach
              </div>
              <div class="swiper-pagination"></div>
            </div>
          </div>

          <div class="col-lg-4">
            <div class="portfolio-info">
              <h3>Project information</h3>
              <ul>
                <li><strong>Product Name</strong>: {{ $product->name }}</li>
                <li><strong>Category</strong>: {{ $product->category->name }}</li>
              </ul>
            </div>
            <div class="portfolio-description">
                <h2>Product Description</h2>
                <hr/>
              <p>
                {!! $product->description !!}
              </p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Portfolio Details Section -->

  </main><!-- End #main -->


@endsection