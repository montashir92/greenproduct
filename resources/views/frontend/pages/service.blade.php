@extends('frontend.layouts.master')

@section('title') GreenLeaf - Products @endsection

@section('content')



<main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
        <div class="container">

            <div class="d-flex justify-content-between align-items-center">
                <h2>Products</h2>
                <ol>
                    <li><a href="{{ route('index') }}">Home</a></li>
                    <li>Products</li>
                </ol>
            </div>

        </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services section-bg">
        <div class="container" data-aos="fade-up">

            <div class="row">
                @foreach($products as $product)
                <div class="col-lg-3 col-md-4 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
                    <div class="card mb-4">
                        <a href="{{ route('product.details', $product->id) }}">
                            <img src="{{ asset('backend/images/products/'.$product->image) }}" height="250" width="100%" class="card-img-top" alt="...">
                        </a>

                        <div class="card-body text-center">
                            <h5>{{ $product->name }}</h5>
                            <a href="{{ route('product.details', $product->id) }}" class="btn btn-primary btn-sm">View Details</a>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>

        </div>
    </section><!-- End Services Section -->



</main><!-- End #main -->


@endsection