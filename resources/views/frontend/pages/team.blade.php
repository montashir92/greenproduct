@extends('frontend.layouts.master')

@section('title') GreenLeaf - Our Team @endsection

@section('content')


<main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
        <div class="container">

            <div class="d-flex justify-content-between align-items-center">
                <h2>Team</h2>
                <ol>
                    <li><a href="{{ route('index') }}">Home</a></li>
                    <li>Team</li>
                </ol>
            </div>

        </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Our Team Section ======= -->
    @include('frontend.partials.teams')

</main><!-- End #main -->


@endsection