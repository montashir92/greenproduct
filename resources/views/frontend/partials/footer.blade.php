
@php
    $setting = App\Models\Setting::find(3);
    $products = App\Models\Product::orderBy('id', 'DESC')->limit(6)->get();
    $photogalleries = App\Models\ImageGallery::orderBy('id', 'DESC')->limit(4)->get();
    $blogs = App\Models\Newwsevent::orderBy('id', 'DESC')->paginate(6);
@endphp

<footer id="footer">

    <div class="footer-top">
        <div class="container">
            <div class="row">

                <div class="col-lg-3 col-md-6 footer-links">
                    <h4>Products</h4>
                    <ul>
                        @foreach($products as $product)
                        <li><i class="bx bx-chevron-right"></i> <a href="{{ route('product.details', $product->id) }}">{{ $product->name }}</a></li>
                        @endforeach
                    </ul>
                </div>

                <div class="col-lg-3 col-md-6 footer-links">
                    <h4>News & Event</h4>
                    <ul>
                        @foreach($blogs as $blog)
                        <li><i class="bx bx-chevron-right"></i> <a href="{{ route('blog.details', $blog->id) }}">{{ $blog->title }}</a></li>
                        @endforeach
                    </ul>
                </div>

                <div class="col-lg-3 col-md-6 footer-links">
                    <h4>Photo Gallery</h4>
                    @foreach($photogalleries as $photogallery)
                    <img src="{{ asset('backend/images/galleries/'.$photogallery->image) }}" style="width: 120px; margin: 3px; background: #f5f5f0; padding: 3px; border: 1px solid #444;" alt="">
                    @endforeach
                </div>
                
                <div class="col-lg-3 col-md-6 footer-links">
                    <h4>Address</h4>
                    <p style="color: rgba(255, 255, 255, 0.6);">
                        {{ $setting->address }}
                        <br><br>
                        <strong>Phone:</strong> {{ $setting->phone }}<br>
                        <strong>Email:</strong> {{ $setting->email }}<br>
                    </p>
                </div>


            </div>
        </div>
    </div>

    <div class="container d-lg py-4 botton_footer">
        <link href="../../../../public/frontend/assets/css/style.css" rel="stylesheet" type="text/css"/>

        <div class="me-md-auto text-center text-md-start">
            <div class="copyright">
                &copy; Copyright <?php echo date('Y'); ?> <strong><span>Company</span></strong>. All Rights Reserved
            </div>
            <div class="credits">
                <!-- All the links in the footer should remain intact. -->
                <!-- You can delete the links only if you purchased the pro version. -->
                <!-- Licensing information: https://bootstrapmade.com/license/ -->
                <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/company-free-html-bootstrap-template/ -->
                Designed by <a href="https://linktechbd.com/" style="color: lightgreen" target="_blank">Link-Up Technology</a>
            </div>
        </div>
        
    </div>
</footer><!-- End Footer -->