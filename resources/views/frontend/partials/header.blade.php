<header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

        <h1 class="logo me-auto"><a href="{{ route('index') }}"><span>Green</span>Leaf</a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html" class="logo me-auto me-lg-0"><img src="{{ asset('frontend') }}/assets/img/logo.png" alt="" class="img-fluid"></a>-->

        <nav id="navbar" class="navbar order-last order-lg-0">
            <ul>
                <li><a href="{{ route('index') }}" class="{{ Route::is('index') ? 'active' : '' }}">Home</a></li>

                <li class="dropdown"><a href="{{ route('aboutus') }}"><span>About</span> <i class="bi bi-chevron-down"></i></a>
                    <ul>
                        <li><a href="{{ route('aboutus') }}">About Us</a></li>
                        <li><a href="{{ route('teams') }}">Team</a></li>
                        
                    </ul>
                </li>

                <li><a href="{{ route('get.products') }}" class="{{ Route::is('get.products') ? 'active' : '' }}">Products</a></li>
                <li><a href="{{ route('blog.page') }}" class="{{ Route::is('blog.page') ? 'active' : '' }}">News & Event</a></li>
                <li><a href="{{ route('image.galleries') }}" class="{{ Route::is('image.galleries') ? 'active' : '' }}">Photo Gallery</a></li>
                <li><a href="{{ route('contacts') }}" class="{{ Route::is('contacts') ? 'active' : '' }}">Contact</a></li>

            </ul>
            <i class="bi bi-list mobile-nav-toggle"></i>
        </nav><!-- .navbar -->


    </div>
</header><!-- End Header -->