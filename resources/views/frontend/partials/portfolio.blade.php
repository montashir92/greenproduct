<section id="portfolio" class="portfolio">
    <div class="container">

        <div class="row" data-aos="fade-up">
            <div class="col-lg-12 d-flex justify-content-center">
                <ul id="portfolio-flters">
                    <li data-filter="*" class="filter-active">All</li>
                    @foreach($categories as $category)
                    <li data-filter=".filter-app{{ $category->id }}">
                        <a href="" class="">{{ $category->name }}</a>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>

        <div class="row portfolio-container" data-aos="fade-up">
            @foreach($products as $product)
            <div class="col-lg-4 col-md-6 portfolio-item filter-app{{ $product->category_id }}">
                <a href="{{ route('product.details', $product->id) }}">
                    <img src="{{ asset('backend/images/products/'.$product->image) }}" class="img-fluid" alt="">
                </a>
                <div class="portfolio-info">
                    <h4>{{ $product->name }}</h4>
                    <a href="{{ route('product.details', $product->id) }}" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
                </div>
            </div>
            @endforeach
        </div>

    </div>
</section><!-- End Portfolio Section -->