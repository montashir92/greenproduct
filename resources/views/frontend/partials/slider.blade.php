

<section id="hero">
    <div id="heroCarousel" data-bs-interval="5000" class="carousel slide carousel-fade" data-bs-ride="carousel">
        
        <div class="carousel-inner" role="listbox">

            <!-- Slide -->
            @foreach($sliders as $slider)
            <div class="carousel-item {{ $loop->index == 0 ? 'active' : '' }}" style="backgorund-position:center center; background-image: url({{ asset('backend/images/sliders/'.$slider->image) }}); width: 100%;">
                <div class="carousel-container">
                    <div class="carousel-content animate__animated animate__fadeInUp">
                        <h2><span>{{ $slider->title }}</span></h2>
                        <p>{!! $slider->description !!}</p>
                        <div class="text-center"><a href="{{ route('aboutus') }}" class="btn-get-started">Read More</a></div>
                    </div>
                </div>
            </div>
            @endforeach

        </div>
        

        <a class="carousel-control-prev" href="#heroCarousel" role="button" data-bs-slide="prev">
            <span class="carousel-control-prev-icon bi bi-chevron-left" aria-hidden="true"></span>
        </a>

        <a class="carousel-control-next" href="#heroCarousel" role="button" data-bs-slide="next">
            <span class="carousel-control-next-icon bi bi-chevron-right" aria-hidden="true"></span>
        </a>

        <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>

    </div>
</section><!-- End Hero -->