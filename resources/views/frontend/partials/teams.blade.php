<section id="team" class="team section-bg">
    <div class="container">

        <div class="section-title" data-aos="fade-up">
            <h2>Our <strong>Team</strong></h2>
            <p></p>
        </div>

        <div class="row">
            @foreach($teams as $team)
            <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                <div class="member" data-aos="fade-up">
                    <div class="member-img">
                        <img src="{{ asset('backend/images/teams/'.$team->image) }}" class="img-fluid" alt="">
                        <div class="social">
                            <a href="{{ $team->twitter }}" target="_blank"><i class="bi bi-twitter"></i></a>
                            <a href="{{ $team->facebook }}" target="_blank"><i class="bi bi-facebook"></i></a>
                            <a href="{{ $team->instagram }}" target="_blank"><i class="bi bi-instagram"></i></a>
                            <a href="{{ $team->linkedin }}" target="_blank"><i class="bi bi-linkedin"></i></a>
                        </div>
                    </div>
                    <div class="member-info">
                        <h4>{{ $team->name }}</h4>
                        <span>{{ $team->designation }}</span>
                    </div>
                </div>
            </div>
            @endforeach


        </div>

    </div>
</section><!-- End Our Team Section -->
