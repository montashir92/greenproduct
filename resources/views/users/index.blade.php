@extends('backend.layouts.admin_master')


@section('admin_content')



<nav class="breadcrumb sl-breadcrumb">
    <a class="breadcrumb-item" href="">GreenProduct</a>
    <span class="breadcrumb-item active">Categories</span>
</nav>

<div class="sl-pagebody">
    <div class="row row-sm">
        <div class="col-sm-4">
            <div class="card" style="width: 18rem;">
                <img class="card-img-top round_image" src="{{ (!empty(Auth::user()->image)) ? asset('backend/images/users/'.Auth::user()->image) : asset('backend/images/users/user.jpg') }}" width="100%" height="100%" alt="Card image cap">
                <ul class="list-group list-group-flush">
                    <a href="{{ route('user.profile') }}" class="btn btn-primary btn-sm btn-block">Home</a>
                    <a href="{{ route('user.change.password') }}" class="btn btn-primary btn-sm btn-block">Change Password</a>
                    
                </ul>
            </div>
        </div>
        <div class="col-sm-8 mt-1">
                    <div class="card">
                        <h3 class="text-center"><span class="text-danger">Hi...!</span> <strong class="text-warning">{{ Auth::user()->name }}</strong> Update your Profile</h3>
                        <div class="card-body">
                            <form action="{{ route('user.profile.update') }}" method="post" enctype="multipart/form-data">
                                @csrf

                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" name="name" class="form-control" id="name" value="{{ Auth::user()->name }}" aria-describedby="">
                                    @error('name') <span style="color: red">{{$message}}</span> @enderror
                                </div>

                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" name="email" class="form-control" id="email" value="{{ Auth::user()->email }}" aria-describedby="">
                                    @error('email') <span style="color: red">{{$message}}</span> @enderror
                                </div>

                                <div class="form-group">
                                    <label for="phone">Phone</label>
                                    <input type="text" name="phone" class="form-control" id="phone" value="{{ Auth::user()->phone }}" aria-describedby="">
                                    @error('phone') <span style="color: red">{{$message}}</span> @enderror
                                </div>
                                
                                <div class="form-group col-sm-6">
                                    <label for="image">Upload Image</label>
                                    <input type="file" name="image" class="form-control" id="image">
                                    @error('image') <span style="color: red">{{$message}}</span> @enderror
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-success"> Update change</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>

    </div><!-- row -->
</div><!-- sl-pagebody -->
@endsection
