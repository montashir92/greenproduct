<?php

use Illuminate\Support\Facades\Route;

//Frontend
use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Frontend\ContactsController;


//Backend
use App\Http\Controllers\Backend\CategoryController;
use App\Http\Controllers\Backend\ProductController;
use App\Http\Controllers\Backend\SliderController;
use App\Http\Controllers\Backend\AboutController;
use App\Http\Controllers\Backend\HomepageController;
use App\Http\Controllers\Backend\ContactController;
use App\Http\Controllers\Backend\SettingController;
use App\Http\Controllers\Backend\ServiceController;
use App\Http\Controllers\Backend\OurteamController;
use App\Http\Controllers\Backend\ImagegalleryController;
use App\Http\Controllers\Backend\NewseventController;
use App\Http\Controllers\Backend\ClientController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
*/

Route::get('/', [HomeController::class, 'index'])->name('index');
Route::get('/about', [HomeController::class, 'about'])->name('aboutus');
Route::get('/team', [HomeController::class, 'team'])->name('teams');
Route::get('/get/products', [HomeController::class, 'getProduct'])->name('get.products');
Route::get('/gallery/images', [HomeController::class, 'galleryImage'])->name('image.galleries');
Route::get('/blogs', [HomeController::class, 'blog'])->name('blog.page');
Route::get('/blog/details/{id}', [HomeController::class, 'blogDetails'])->name('blog.details');
Route::get('/contact', [HomeController::class, 'contact'])->name('contacts');

//Product Details Routes
Route::get('/details/product/{id}', [HomeController::class, 'show'])->name('product.details');

//Contact Message Routes
Route::post('/message/send', [ContactsController::class, 'store'])->name('message.send');



Auth::routes();

Route::group(['middleware' => 'auth'], function(){    
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/user/profile', [App\Http\Controllers\HomeController::class, 'profile'])->name('user.profile');
    Route::post('/user/profile/update', [App\Http\Controllers\HomeController::class, 'profileUpdate'])->name('user.profile.update');
    Route::get('/change/password', [App\Http\Controllers\HomeController::class, 'editPassword'])->name('user.change.password');
    Route::post('/change/password/update', [App\Http\Controllers\HomeController::class, 'updatePassword'])->name('user.update.password');
    

    //Category Routes
    Route::get('/categories', [CategoryController::class, 'index'])->name('admin.categories');
    Route::get('/category/create', [CategoryController::class, 'create'])->name('admin.category.create');
    Route::post('/category/store', [CategoryController::class, 'store'])->name('admin.category.store');
    Route::get('/category/edit/{id}', [CategoryController::class, 'edit'])->name('admin.category.edit');
    Route::post('/category/update/{id}', [CategoryController::class, 'update'])->name('admin.category.update');
    Route::post('/category/delete', [CategoryController::class, 'delete'])->name('admin.category.delete');
    
    //Product Routes
    Route::get('/products', [ProductController::class, 'index'])->name('admin.products');
    Route::get('/product/create', [ProductController::class, 'create'])->name('admin.product.create');
    Route::post('/product/store', [ProductController::class, 'store'])->name('admin.product.store');
    Route::get('/product/show/{id}', [ProductController::class, 'show'])->name('admin.product.show');
    Route::get('/product/edit/{id}', [ProductController::class, 'edit'])->name('admin.product.edit');
    Route::post('/product/update/{id}', [ProductController::class, 'update'])->name('admin.product.update');
    Route::post('/product/delete', [ProductController::class, 'delete'])->name('admin.product.delete');
    
    //Slider Routes
    Route::get('/sliders', [SliderController::class, 'index'])->name('admin.sliders');
    Route::get('/slider/create', [SliderController::class, 'create'])->name('admin.slider.create');
    Route::post('/slider/store', [SliderController::class, 'store'])->name('admin.slider.store');
    Route::get('/slider/edit/{id}', [SliderController::class, 'edit'])->name('admin.slider.edit');
    Route::post('/slider/update/{id}', [SliderController::class, 'update'])->name('admin.slider.update');
    Route::post('/slider/delete', [SliderController::class, 'delete'])->name('admin.slider.delete');
    
    //AboutUs Routes
    Route::get('/abouts', [AboutController::class, 'index'])->name('admin.abouts');
    Route::get('/about/create', [AboutController::class, 'create'])->name('admin.about.create');
    Route::post('/about/store', [AboutController::class, 'store'])->name('admin.about.store');
    Route::get('/about/edit/{id}', [AboutController::class, 'edit'])->name('admin.about.edit');
    Route::post('/about/update/{id}', [AboutController::class, 'update'])->name('admin.about.update');
    Route::post('/about/delete', [AboutController::class, 'delete'])->name('admin.about.delete');
    
    
    //Contact Routes
    Route::get('/contacts', [ContactController::class, 'index'])->name('admin.contacts');
    Route::post('/contact/delete', [ContactController::class, 'delete'])->name('admin.contact.delete');
    

    //Setting Routes
    Route::get('/settings', [SettingController::class, 'index'])->name('admin.settings');
    Route::get('/edit/{id}', [SettingController::class, 'edit'])->name('admin.setting.edit');
    Route::post('/setting/update/{id}', [SettingController::class, 'update'])->name('admin.setting.update');
    
    
    //Ourteam Routes
    Route::get('/ourteams', [OurteamController::class, 'index'])->name('admin.ourteams');
    Route::get('/ourteam/create', [OurteamController::class, 'create'])->name('admin.ourteam.create');
    Route::post('/ourteam/store', [OurteamController::class, 'store'])->name('admin.ourteam.store');
    Route::get('/ourteam/edit/{id}', [OurteamController::class, 'edit'])->name('admin.ourteam.edit');
    Route::post('/ourteam/update/{id}', [OurteamController::class, 'update'])->name('admin.ourteam.update');
    Route::post('/ourteam/delete', [OurteamController::class, 'delete'])->name('admin.ourteam.delete');
    
    //Photo Gallery Routes
    Route::get('/photos', [ImagegalleryController::class, 'index'])->name('admin.photos');
    Route::get('/photo/create', [ImagegalleryController::class, 'create'])->name('admin.photo.create');
    Route::post('/photo/store', [ImagegalleryController::class, 'store'])->name('admin.photo.store');
    Route::get('/photo/edit/{id}', [ImagegalleryController::class, 'edit'])->name('admin.photo.edit');
    Route::post('/photo/update/{id}', [ImagegalleryController::class, 'update'])->name('admin.photo.update');
    Route::post('/photo/delete', [ImagegalleryController::class, 'delete'])->name('admin.photo.delete');
    
    //News & Event Routes
    Route::get('/newsevents', [NewseventController::class, 'index'])->name('admin.newsevents');
    Route::get('/newsevent/create', [NewseventController::class, 'create'])->name('admin.newsevent.create');
    Route::post('/newsevent/store', [NewseventController::class, 'store'])->name('admin.newsevent.store');
    Route::get('/newsevent/edit/{id}', [NewseventController::class, 'edit'])->name('admin.newsevent.edit');
    Route::post('/newsevent/update/{id}', [NewseventController::class, 'update'])->name('admin.newsevent.update');
    Route::post('/newsevent/delete', [NewseventController::class, 'delete'])->name('admin.newsevent.delete');
    
    //Client Routes
    Route::get('/clients', [ClientController::class, 'index'])->name('admin.clients');
    Route::get('/client/create', [ClientController::class, 'create'])->name('admin.client.create');
    Route::post('/client/store', [ClientController::class, 'store'])->name('admin.client.store');
    Route::get('/client/edit/{id}', [ClientController::class, 'edit'])->name('admin.client.edit');
    Route::post('/client/update/{id}', [ClientController::class, 'update'])->name('admin.client.update');
    Route::post('/client/delete', [ClientController::class, 'delete'])->name('admin.client.delete');
    
});
